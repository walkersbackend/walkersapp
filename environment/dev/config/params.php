<?php
return [
    'adminEmail' => 'admin@example.com',
    'localUploadDirectory' => '/var/www/uploads_walkers_test/',
    'globalUploadDirectory' => 'http://walkers-msk.vl.ru/uploads/'
];
