<?php
return [
    'adminEmail' => 'admin@example.com',
    'localUploadDirectory' => '/var/www/uploads_walkers/',
    'globalUploadDirectory' => 'http://walkers.vl.ru/uploads/'
];
