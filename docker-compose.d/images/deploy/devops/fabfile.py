# -*- coding: utf-8 -*-
import os
import time

from fabric.api import local, env, put, run, cd, lcd
from fabric.contrib.files import exists

CURRENT_PATH = os.path.dirname(os.path.realpath(__file__))

def build(env='prod'):
	with lcd('build_folder/basic'):
		local('composer install --no-interaction --no-dev')
	local('rm -f walkers.tar.gz')
	local('cp -Rf ./build_folder/environment/{env}/* ./build_folder/basic/ > /dev/null'.format(env=env))
	local('cp -Rf ./build_folder/environment/replace/PolylineOptions.php ./build_folder/basic/vendor/2amigos/yii2-google-maps-library/overlays/ > /dev/null')
	local('rm -Rf ./build_folder/environment')
	local('tar cvzf walkers.tar.gz -C build_folder/basic . > /dev/null')


def deploy(remote_projects_path,project,build_num=-1):
	if int(build_num) == -1:
		build_num = int(time.time())
	if not exists('{remote_projects}/{project}'.format(remote_projects=remote_projects_path, project=project)):
		run('mkdir -p {remote_projects}/{project}'.format(remote_projects=remote_projects_path, project=project))
	with cd('{remote_projects}/{project}'.format(remote_projects=remote_projects_path, project=project)):
		if not exists('./{project}-{build}'.format(project=project,build=build_num)):
			run('mkdir -p ./tmp'.format(remote_projects=remote_projects_path, project=project))
			put(
				'walkers.tar.gz'.format(build=build_num),
				'./tmp'.format(remote_projects=remote_projects_path, project=project)
			)
			run('mkdir -p ./{project}-{build}'.format(project=project,build=build_num))
			run('tar xf tmp/walkers.tar.gz -C ./{project}-{build}'.format(project=project,build=build_num))
			run('rm -rf ./tmp')
		if exists('./current'):
			run('unlink ./current')
		run('ln -s ./{project}-{build} ./current'.format(project=project,build=build_num))
		if exists('./current/web/uploads'):
			run('unlink ./current/web/uploads')
		run('ln -s ./../../../uploads_{project} ./current/web/uploads'.format(project=project))
		run('/opt/php-7.1/bin/php ./current/yii migrate')
		if exists('./build'):
			run('mv ./build ./rollback')
		run('echo {build} > ./build'.format(build=build_num))
		run('ls -1 --sort=t | grep {project} | tail -n+6 | xargs rm -rf'.format(project=project))


def rollback(remote_projects_path,project):
	with cd('{remote_projects}/{project}'.format(remote_projects=remote_projects_path, project=project)):
		if not exists('./rollback'):
			print 'Нет файла rollback, используйте деплой для отката'
			return
		run('unlink ./current')
		run('ln -s ./{project}-$(cat ./rollback) ./current'.format(project=project))
		run('mv ./rollback ./build')