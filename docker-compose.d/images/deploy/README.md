1. Предполагается, что собран и запущен контейнер ``deploy``

2. Переходим в корень проекта 
    ```sh
    cd {path-to-walkersapp}
    ```
    
3. Заходим в контейнер ``deploy``
    ```sh
    sudo docker-compose exec deploy bash
    ```
    Будет что-то типа ``root@e640f0f812d0:/#``
    
4. Стягиваем нужную ветку
    ```sh
    fab -f ./prepare.py pull:[название ветки]
    ```
    
5. Собираем проект
    ```sh
    fab -f ./prepare.py build:[dev|prod-по умолчанию]
    ```
    
6. Запускаем сценарий деплоя
    ```sh
    fab -f ./prepare.py deploy
    ```
    
7. Если понадобится откатить только что залитый проект, к предыдущему состоянию
    ```sh
    fab -f ./prepare.py rollback
    ```
    
8. Выходим из контейнера
    ```sh
    exit
    ```