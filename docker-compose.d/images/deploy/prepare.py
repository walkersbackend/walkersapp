import os
import json
from fabric.api import local, lcd


def pull(branch):
	with lcd('/devops'):
		local('rm -rf build_folder')
		# build commands
		local('git clone -b {branch} {git_address} build_folder'.format(
			git_address='git@bitbucket.org:walkersbackend/walkersapp.git',
			branch=branch
		))
		local('rm -f ./fabfile.py')
		local('ln -s ./build_folder/docker-compose.d/images/deploy/devops/fabfile.py ./fabfile.py')


def build(env='prod'):
	generateConfig(env)
	with lcd('/devops'):
		local('fab build:{env}'.format(env=env))


def deploy(build_num=-1):
	with lcd('/devops'):
		local('fab {command}'.format(command=printCommand('deploy')))


def rollback():
	with lcd('/devops'):
		local('fab {command}'.format(command=printCommand('rollback')))


def readJson(fileName):
	return json.loads(readFile(fileName))


def readFile(fileName):
	file = open(fileName)
	str = file.read()
	file.close()
	return str


def generateConfig(env):
	_json = readJson('/devops/build_folder/docker-compose.d/images/deploy/config.json')
	path = _json.pop('environment')[env]
	_json.update({'project':path})
	with open('/devops/build_folder/docker-compose.d/images/deploy/config.json', "w") as file:
		json.dump(_json, file)


def printCommand(command):
	_json = readJson('/devops/build_folder/docker-compose.d/images/deploy/config.json')
	return '-H {host} -u {user} {command}:{remote_projects_path},{project}'.format(
		host=_json['host'],
		user=_json['user'],
		command=command,
		remote_projects_path=_json['remote_projects_path'],
		project=_json['project']
	)