# Nginx PHP+Composer PhpMyAdmin MySQL Deploy(Python+PHP-CLI+Fabric)[![Build Status](https://travis-ci.org/nanoninja/docker-nginx-php-mysql.svg?branch=master)](https://travis-ci.org/nanoninja/docker-nginx-php-mysql) [![GitHub version](https://badge.fury.io/gh/nanoninja%2Fdocker-nginx-php-mysql.svg)](https://badge.fury.io/gh/nanoninja%2Fdocker-nginx-php-mysql)

Docker running Nginx, PHP-FPM, Composer, MySQL, PHPMyAdmin and Deploy container.

## Overview

1. [Install prerequisites](#install-prerequisites)

    Before installing project make sure the following prerequisites have been met.

2. [Clone the project](#clone-the-project)

    We’ll download the code from its repository on GitHub.

3. [Configure Nginx](#configure-nginx) [Optional]

    We'll configure host name before running server.

4. [Configure Xdebug](#configure-xdebug) [Optional]

    We'll configure Xdebug for IDE (PHPStorm or Netbeans).

5. [Run the application](#run-the-application)

    By this point we’ll have all the project pieces in place.

6. [Use Docker Commands](#use-docker-commands)

    When running, you can use docker commands for doing recurrent operations.
    
7. [After all](#swagger-install)
    
    We'll install swagger.

___

## Install prerequisites

For now, this project has been mainly created for Unix `(Linux/MacOS)`. Perhaps it could work on Windows.

All requisites should be available for your distribution. The most important are :

* [Git](https://git-scm.com/downloads)
* [Docker](https://docs.docker.com/engine/installation/) use version > 17.05
* [Docker Compose](https://docs.docker.com/compose/install/)

Check if `docker-compose` is already installed by entering the following command : 

```sh
which docker-compose
```

### Images to use

* [Nginx](https://hub.docker.com/_/nginx/)
* [MySQL](https://hub.docker.com/_/mysql/)
* [PHP-FPM](https://hub.docker.com/_/php/)
* [PHPMyAdmin](https://hub.docker.com/r/phpmyadmin/phpmyadmin/)
* [PHP-CLI](https://hub.docker.com/_/php/)

You should be careful when installing third party web servers such as MySQL or Nginx.

This project use the following ports :

| Server     | Port |
|------------|------|
| MySQL      | 3007 |
| PHPMyAdmin | 8183 |
| Nginx      | 8000 |
| PHP-FPM    | 9000 |
---

## Clone the project

To install [Git](http://git-scm.com/book/en/v2/Getting-Started-Installing-Git), download it and install following the instructions : 

```sh
git clone https://{your bitbucket username}@bitbucket.org/walkersbackend/walkersapp.git
```

Go to the project directory : 

```sh
cd walkersapp
```

### Project tree

```sh
.
├── basic
├── docker-compose.d
│   ├── data
│   │   ├── mysql
│   │   │   ├── db
│   │   │   └── dumps
│   │   ├── nginx
│   │   │   └── logs
│   ├── etc
│   │   ├── nginx
│   │   │   └── default.template
│   │   ├── php
│   │   │   └── php.ini
│   └── images
│       ├── deploy
│       │   ├── devops
│       │   │   └── fabfile.py
│       │   ├── keys
│       │   │   ├── id_rsa
│       │   │   └── id_rsa.pub
│       │   ├── config.json
│       │   ├── DockerFile
│       │   ├── prepare.py
│       │   └── README.MD
│       └── php
│           └── Dockerfile
├── environment
├── swagger
├── swagger-editor
├── .env
├── docker-compose.yml
└── README.md
```

---

## Configure Nginx

You can change the host name by editing the `.env` file.

If you modify the host name, do not forget to add it to the `/etc/hosts` file.

    Configure Nginx

    Do not modify the `etc/nginx/default.template` file, it is overwritten by .env file variables
    and will be use as default config of the project
    
---

## Configure Xdebug

If you use another IDE than [PHPStorm](https://www.jetbrains.com/phpstorm/) or [Netbeans](https://netbeans.org/), go to the [remote debugging](https://xdebug.org/docs/remote) section of Xdebug documentation.

For a better integration of Docker to PHPStorm, use the [documentation](https://github.com/nanoninja/docker-nginx-php-mysql/blob/master/doc/phpstorm-macosx.md).

1. In `etc/php/php.ini` port set's to `9001`, \`cause usually port `9000` is taken by `php`: 
    so don't forget change it in IDE settings

---

## Run the application

1. Start the application :

    ```sh
    sudo docker-compose up -d
    ```

    **Please wait this might take a several minutes...**

    ```sh
    sudo docker-compose logs -f # Follow log output
    ```

2. Open your favorite browser :

    * [http://{host name by default is `walkersapp`}:8000](http://walkersapp:8000/)
    * [http://{mysql host by default is `mysql`}:8183](http://mysql:8183/) PHPMyAdmin (username: root, password: walkers)

3. Stop and clear services

    ```sh
    sudo docker-compose down -v
    ```
    
    if you want clear images use :
    
    ```sh
    sudo docker-compose down -v --rmi all
    ```
    
    remove all folders and files:
    
    ```sh
    ./clear.sh
    ```
---
## Use Docker Commands

### Handling database

#### MySQL shell access

Firstly edit `etc/hosts` add:

```sh
127.0.0.1 $MYSQL_HOST
```

then

```sh
sudo docker-compose exec mysql bash
```

and

```sh
mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD"
```

if you want connect to mysql from host-machine use:

```sh
mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" -P3307 -h"$MYSQL_HOST"
```

#### Backup of database

1. From host-machine

    ```sh
    source .env && sudo docker-compose exec mysql mysqldump --all-databases -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "/dump/db.sql"
    ```

    or for specific db

    ```sh
    source .env && sudo docker-compose exec mysql mysqldump sprecific_db -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "/dump/specific_db.sql"
    ```

2. In mysql container

    ```sh
    mysqldump --all-databases -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "/dump/db.sql"
    ```
    
    or for specific db
    
    ```sh
    mysqldump sprecific_db -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" > "/dump/specific_db.sql"
    ```
    
#### Restore Database

1. From host-machine

    ```sh
    source .env && sudo docker-compose exec mysql mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" < "/dump/db.sql"
    ```
    
2. In mysql container

    ```sh
    mysql -u"$MYSQL_ROOT_USER" -p"$MYSQL_ROOT_PASSWORD" < "/dump/db.sql"
    ```

#### Connecting MySQL from [PDO](http://php.net/manual/en/book.pdo.php)

```php
<?php
    try {
        $dsn = 'mysql:host=mysql;dbname=walkers;charset=utf8;port=3306';
        $pdo = new PDO($dsn, 'root', 'walkers');
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
?>
```

---

## Deploy

Follow by instructions in [Deploy readme](docker-compose.d/images/deploy/README.md)

---

# Swagger install

1. Run swagger.sh, script create [swagger-ui](https://github.com/swagger-api/swagger-ui) and [swagger-editor](https://github.com/swagger-api/swagger-editor)
    
    ```sh
    ./swagger.sh
    ```