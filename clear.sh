#!/bin/bash

sudo rm -rf ./swagger ./swagger-editor basic/vendor

cd ./docker-compose.d

sudo rm -rf \
data/mysql/db/* \
data/mysql/dump/* \
data/nginx/logs/*

cd etc/nginx && sudo rm $(ls | grep -v default)