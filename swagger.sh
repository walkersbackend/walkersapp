#!/bin/bash

rm $(ls | grep .zip)
rm -rf swagger/* swagger-editor/*

ui_url=https://github.com/swagger-api/swagger-ui/archive/master.zip
editor_url=https://github.com/swagger-api/swagger-editor/archive/master.zip

wget ${ui_url} \
    && unzip -o master.zip \
    && cp ./swagger-ui-master/dist/index.html ./swagger-ui-master/dist/project.html \
    && sed -i 's/\(url:\).*,/\1\"\/swagger.json\",/' ./swagger-ui-master/dist/index.html \
    && sed -i 's/\(url:\).*,/\1\"\/project.json\",/' ./swagger-ui-master/dist/project.html \
    && sudo cp -rf ./swagger-ui-master/* ./swagger/ \
    && rm -rf ./swagger-ui-master/ ./master.zip
wget ${editor_url} \
    && unzip -o master.zip \
    && sudo cp -rf ./swagger-editor-master/* ./swagger-editor/ \
    && rm -rf ./swagger-editor-master/ ./master.zip
