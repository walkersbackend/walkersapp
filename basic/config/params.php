<?php
return [
    'adminEmail' => 'admin@example.com',
    'localUploadDirectory' => '/var/www/walkersapp/basic/web/uploads/',
    'globalUploadDirectory' => 'walkersapp/uploads/'
];
