<?php

use app\modules\api\modules\client\services\auth\AbstractWalkerAuthService;
use app\modules\api\modules\client\services\auth\WalkerAuthService;
use app\modules\api\modules\client\services\task\back\ITaskReturnerService;
use app\modules\api\modules\client\services\task\back\TaskReturnerService;
use app\modules\api\modules\client\services\task\complete\ITaskCompletionService;
use app\modules\api\modules\client\services\task\complete\TaskCompletionService;
use app\modules\api\modules\client\services\task\find\ITaskFinderService;
use app\modules\api\modules\client\services\task\find\TaskFinderService;

Yii::$container->set(AbstractWalkerAuthService::class, WalkerAuthService::class);
Yii::$container->set(ITaskFinderService::class, TaskFinderService::class);
Yii::$container->set(ITaskCompletionService::class, TaskCompletionService::class);
Yii::$container->set(ITaskReturnerService::class, TaskReturnerService::class);