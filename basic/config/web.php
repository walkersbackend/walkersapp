<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'ru',
    'aliases' => [
        '@uploads' => $params['localUploadDirectory'],
        '@uploadsGlobal' => $params['globalUploadDirectory'],
    ],
    'modules' => [
        'api' => [
            'class' => 'app\modules\api\Module',
        ],
        'dashboard' => [
            'class' => 'app\modules\dashboard\Module',
        ],
        'access' => [
            'class' => 'app\modules\access\Module',
        ],
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'jKLU0kdMODoJRJMXmnu-JwBOB2xZZ7pn',
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => \yii\web\JsonParser::class
            ]
        ],
        'assetManager' => [
            'bundles' => [
                'dosamigos\google\maps\MapAsset' => [
                    'options' => [
                        'key' => 'AIzaSyA6s8f7rr-2ntq0p4d4GxWuVzQgBCOurjE',
                        'language' => 'ru',
                        'version' => '3.1.18'
                    ]
                ]
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\access\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                'file' => [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
                'graylog' => [
                    'class' => 'nex\graylog\GraylogTarget',
                    'levels' => ['info'],
                    'categories' => ['application'],
                    'logVars' => [], // This prevent yii2-debug from crashing ;)
                    'host' => 'graylog2dev-msk.vl.ru',
                    'additionalFields' => [
                        'method' => function($yii) {
                            return $yii->request->getMethod();
                        },
                        'post' => function($yii) {
                            return $yii->request->isPost() ? $yii->request->post() : '';
                        },
                        'request-uri' => function($yii) {
                            return $yii->request->getUrl();
                        },
                        'user-ip' => function($yii) {
                            return $yii->request->getUserIP();
                        },
                        'user-agent' => function($yii) {
                            return $yii->request->getUserAgent();
                        },
                        'content-type' => function($yii) {
                            return $yii->request->getContentType();
                        },
                    ]
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'dashboard/map/<id:\d+>' => 'dashboard/map/index'
            ],
        ]
    ],
    'params' => $params,
];


if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
