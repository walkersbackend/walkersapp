<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
$this->title = 'Ходоки';
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'Главная',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    if (!Yii::$app->user->isGuest) {
        $items = [
            [
                'label' => 'Управление',
                'items' => [
                    [
                        'label' => 'Задачи',
                        'url' => createUrl('dashboard/task')
                    ],
                    [
                        'label' => 'Ходоки',
                        'url' => createUrl('dashboard/walker')
                    ],
                ],
            ],
            [
                'label' => 'Отчеты',
                'items' => [
                    [
                        'label' => 'Динамика по задачам',
                        'url' => createUrl('dashboard/statistics/tasks')
                    ],
                    [
                        'label' => 'Динамика по ходокам',
                        'url' => createUrl('dashboard/statistics/walkers')
                    ],
                    [
                        'label' => 'Объемы выполнения ходоков',
                        'url' => createUrl('dashboard/statistics/walkers-results')
                    ],
                ],
            ]
        ];
        $API = [
            'label' => 'SWAGGER',
            'items' => [
                [
                    'label' => 'API Client',
                    'url' => createUrl('dashboard/swagger/?path=client')
                ],
                [
                    'label' => 'API Project',
                    'url' => createUrl('dashboard/swagger/?path=project')
                ]
            ],
        ];

        if(YII_ENV == 'dev') {
            $API['items'][] = [
                'label' => 'Editor',
                'url' => createUrl('dashboard/swagger/?path=editor')
            ];
        }

        $items[] = $API;
        $items[] = ['label' => 'Карта', 'url' => createUrl('dashboard/map/active')];
        $items[] = ['label' => 'Галерея', 'url' => ('/dashboard/gallery')];
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-left'],
            'items' => $items,
        ]);
    }
    $authLabel = [
            'label' => !Yii::$app->user->isGuest ? 'Выйти' : 'Войти',
            'url' => Yii::$app->getUrlManager()->createUrl('access/main/log' . (!Yii::$app->user->isGuest ? 'out' : 'in'))
    ];
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [$authLabel],
    ]);

    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Ходоки <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<?php
/**
 * @param $path
 * @return string
 */
function createUrl($path) {
    return Yii::$app->getUrlManager()->createUrl($path);
}
?>