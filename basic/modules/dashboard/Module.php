<?php

namespace app\modules\dashboard;

/**
 * Dashboard module definition class
 */
class Module extends \yii\base\Module {
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\dashboard\controllers';
}
