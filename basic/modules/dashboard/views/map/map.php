
<?php
/* @var $tasks app\modules\api\models\task\Task*/
/* @var $histories app\modules\api\models\history\History*/

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;

?>
<?php
$availableTaskIcon = '/assets/icons/availableTaskMarker.png';
$executingTaskIcon = '/assets/icons/executingTaskMarker.png';
$walkerIcon = '/assets/icons/walkerMarker.png';

$coord = new LatLng(['lat' => $tasks[0]->latitude, 'lng' => $tasks[0]->longitude]);
$map = new Map([
    'center' => $coord,
    'zoom' => 13,
    'width' => '100%',
    'height' => '100%'
]);

foreach ($tasks as $task) {
    if ($task->latitude == 0.0 && $task->longitude == 0.0) {
        continue;
    }
    $marker = new Marker([
        'position' => new LatLng(['lat' => $task->latitude, 'lng' => $task->longitude]),
        'title' => $task->title,
        'icon' => $task->status == \app\modules\api\models\constants\status\Status::AVAILABLE ? $availableTaskIcon : $executingTaskIcon
    ]);
    $content = '<address><strong>' .  $task->title . '</strong>';
    if(!empty($task->address)) $content = $content . '<br>' . $task->address;
    $content = $content . '<br>Добавлена : ' . date("Y-m-d H:i", $task->date);
    $content = $content . '</address>';

    $marker->attachInfoWindow(new InfoWindow([
        'content' => $content
    ]));

    $map->addOverlay($marker);
}

foreach ($histories as $history) {
    $track = $history->tracks;
    if(count($track) == 0) continue;
    $lastPos = $track[count($track) - 1];
    $marker = new Marker([
        'position' => new LatLng(['lat' => $lastPos->latitude, 'lng' => $lastPos->longitude]),
        'title' => $history->walker->name,
        'icon' => $walkerIcon
    ]);

    $marker->attachInfoWindow(new InfoWindow([
        'content' => '<address><strong>' . $history->walker->name . ' </strong><br>' .
            '<a href="#"> ' . $history->walker->email . '</a><br/>' .
            'Время последней активности: ' . date('Y-m-d H:i', $lastPos->date) .
            '<br>Текущая задача :<strong>' . $history->task->title . '</strong> ' . (!empty($history->task->address) ? $history->task->address : '') .
            '</address>'
    ]));

    $map->addOverlay($marker);
}

echo $map->display();
