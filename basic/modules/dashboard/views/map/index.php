
<?php
/* @var $model app\modules\api\models\history\History*/

use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\overlays\Polyline;

?>
<?php
$lines = $model->tracks;
if(empty($lines)) {
    echo '<div class="alert alert-danger">К сожалению локаций нет</div>';
    return;
}
$track = [];
foreach ($lines as $line) {
    $track ['coords'][] = new LatLng(array_combine(['lat', 'lng'], [$line->latitude, $line->longitude]));
    $track ['date'][] = date("Y-m-d H:i", $line->date);
}
$start = $track ['coords'][0];
$end = $track ['coords'][count($track ['coords']) - 1];
$startTime = $track ['date'][0];
$endTime = $track ['date'][count($track ['date']) - 1];
$coord = new LatLng(['lat' => 0.5 * ($end->getLat() + $start->getLat()), 'lng' => 0.5 * ($end->getLng() + $start->getLng())]);
$map = new Map([
    'center' => $coord,
    'zoom' => 16,
    'width' => '100%',
    'height' => '100%'
]);

$marker = new Marker([
    'position' => $end,
    'title' => 'Последняя локация',
    'icon' => '/assets/icons/finishMarker.png'
]);

$marker->attachInfoWindow(
    new InfoWindow([
        'content' => '<address><strong>' . $model->walker->name . ' </strong><br>' .
            '<a href="#"> ' . $model->walker->email . '</a><br/>' .
            'Время завершения задания : ' . date('Y-m-d H:i', $model->end_time) . '<br>' .
            'Время последней локации : ' . $endTime . '</address>'
    ])
);

$marker1 = new Marker([
    'position' => $start,
    'title' => 'Первая локация',
    'icon' => '/assets/icons/startMarker.png'
]);

$marker1->attachInfoWindow(
    new InfoWindow([
        'content' => '<address><strong>' . $model->walker->name . ' </strong><br>' .
            '<a href="#"> ' . $model->walker->email . '</a><br/>' .
            'Время взятия задания : ' . date('Y-m-d H:i', $model->start_time) . '<br>' .
            'Время первой локации : ' . $startTime . '</address>'
    ])
);

$task = new Marker([
    'position' => new LatLng(['lat' => $model->task->latitude, 'lng' => $model->task->longitude]),
    'title' => $model->task->title,
    'icon' => '/assets/icons/doneTaskMarker.png'
]);

$content = '<address><strong>' .  $model->task->title . '</strong>';
if(!empty($model->task->address)) $content = $content . '<br>' . $model->task->address;
$content = $content . '<br>Добавлена : ' . date("Y-m-d H:i", $model->task->date);
$content = $content . '</address>';

$task->attachInfoWindow(new InfoWindow([
    'content' => $content
]));


$map->addOverlay($task);
$map->addOverlay($marker);
$map->addOverlay($marker1);

$polyline = new Polyline([
    'path' => $track['coords'],
    'draggable' => false,
    'strokeColor' => '#FFAA00'
]);

$map->addOverlay($polyline);

echo $map->display();
