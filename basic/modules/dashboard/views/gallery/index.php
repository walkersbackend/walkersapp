<?php

/**
 * @var array $images
 */

$this->title = 'Галерея изображений';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="container">
    <?php for ($i = 0, $nextI = $i, $count = count($images); $i < $count; ++$i):?>
        <?php if($i == $nextI): ?>
        <?php $nextI += 4; ?>
            <div class="row">
        <?php endif;?>
        <div class="col-sm-6 col-md-3">
            <div class="thumbnail">
                <?php $image = Yii::getAlias('@uploadsGlobal') . '/' . $images[$i]?>
                <a href="#!" data-toggle="modal" data-target="#myModal">
                    <img src="<?=$image?>" alt="" style="width: 240px;">
                </a>
                <div class="caption text-center">
                    <button data-title="copyToBuffer" data-content="<?=$image?>" class="btn btn-primary">Скопировать ссылку в буфер</button>
                </div>
            </div>
        </div>
        <?php if($i + 1 == $nextI || ($nextI > $count && $i == $count - 1)): ?>
            </div>
        <?php endif;?>
    <?php endfor;?>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Детали загруженного изображения</h4>
            </div>
            <div class="modal-body">
                <img class="img-rounded" id="imagePreviewModal" alt="" src="" width="100%">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>
<?php

$js = <<<JS
   $('button[data-title=copyToBuffer]').click(function() {
       var tmp   = document.createElement('input');
       tmp.value = $(this).attr('data-content');
       document.body.appendChild(tmp);
       tmp.select();
       document.execCommand('copy');
       document.body.removeChild(tmp);
   });
   $('a[data-toggle=modal]').click(function() {
       var img = $('#imagePreviewModal');
       img.attr('src', $(this).children().eq(0).attr('src'));
       img.parent().next().append($(this).next().children().eq(0).clone(true));
   });
   $('#myModal').on('hidden.bs.modal', function () {
      $(this).find('button[data-title=copyToBuffer]').remove();
    });
JS;
$this->registerJs($js, $this::POS_READY);
?>
