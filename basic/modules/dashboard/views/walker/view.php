<?php

use app\modules\api\models\constants\level\Level;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\api\models\walker\Walker */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Walkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="walker-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'level',
                'headerOptions' => ['width' => '150'],
                'filter' => Level::getLabels(),
                'format' => 'html',
                'value' => function($data) {
                    $value = $data->level;
                    switch ($value) {
                        case Level::BASIC:
                            $class = 'default';
                            $label = Level::getLabelByLevel($value);
                            break;
                        case Level::ADVANCED:
                            $class = 'primary';
                            $label = Level::getLabelByLevel($value);
                            break;
                        case Level::EXPERT:
                            $class = 'success';
                            $label = Level::getLabelByLevel($value);
                            break;
                        default:
                            $class = 'warning';
                            $label = 'Не задан';
                    }
                    $html = Html::tag('span', Html::encode($label), ['class' => 'label label-' . $class]);
                    return $html;
                }
            ],
            'name',
            'email:email',
            'number',
            [
                'attribute' => 'date',
                'format' => ['date', 'php:Y-m-d'],
                'headerOptions' => ['width' => '100'],
            ],
            'token',
            'user_agent'
        ],
    ]) ?>

</div>
