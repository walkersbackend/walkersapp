<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\api\models\walker\Walker */

$this->title = 'Create Walker';
$this->params['breadcrumbs'][] = ['label' => 'Walkers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="walker-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
