<?php

use app\modules\api\models\constants\level\Level;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\dashboard\models\walker\WalkerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ходоки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="walker-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Создать ходока', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            // 'id',
            [
                'attribute' => 'level',
                'headerOptions' => ['width' => '150'],
                'filter' => Level::getLabelByLevel(),
                'format' => 'html',
                'value' => function($data) {
                    $value = $data->level;
                    switch ($value) {
                        case Level::BASIC:
                        $class = 'default';
                        $label = Level::getLabelByLevel($value);
                        break;
                    case Level::ADVANCED:
                        $class = 'primary';
                        $label = Level::getLabelByLevel($value);
                        break;
                    case Level::EXPERT:
                        $class = 'success';
                        $label = Level::getLabelByLevel($value);
                        break;
                    default:
                        $class = 'warning';
                        $label = 'Не задан';
                    }
                    $html = Html::tag('span', Html::encode($label), ['class' => 'label label-' . $class]);
                    return $html;
                }
            ],
            'name',
            'email:email',
//            'number',
            'user_agent',
            [
                'attribute' => 'last_activity',
                'format' => ['date', 'php:Y-m-d h:i:s'],
                'headerOptions' => ['width' => '180'],
            ],
            // 'date',
            // 'token',

            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['width' => '70']
            ],
        ],
    ]); ?>
</div>
