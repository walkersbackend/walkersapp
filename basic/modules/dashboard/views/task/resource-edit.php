<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $task app\modules\api\models\task\Task */
/* @var $walkerId int|null */
/* @var $resources \app\modules\dashboard\models\task\TaskResources */

$this->title = $task->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $task->title, 'url' => ['view?id=' . $task->id]];
$this->params['breadcrumbs'][] = 'Ресурсы' . ($walkerId ? ', присланные ходоком' : ' задачи');
if(!$resources->getCount()) {
    echo  '<div class="alert alert-danger">Нет ресурсов</div>';
    return;
}
?>
<?php if( Yii::$app->session->hasFlash('error') ): ?>
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php
            $error = Yii::$app->session->getFlash('error');
            echo is_array($error) ? $error[0] : $error;
        ?>
    </div>
<?php endif;?>
<?php echo $this->render('image-upload', ['url' => '/api/client/upload/file'])?>
<?php $fieldsArray = \app\modules\api\models\constants\resource\Resource::getFieldIds(); ?>
<div class="task-form">
    <?php $form = ActiveForm::begin(); ?>
    <?php for ($i = 0; $i < $resources->getCount(); ++$i):?>
        <?php $res = $resources->resources[$i]; ?>
        <?= Html::beginTag('div', ['class' => 'panel panel-primary']) ?>
        <?= Html::beginTag('div', ['class' => 'panel panel-heading']) ?>
        <?= Html::tag('h3', $res->field_name . "($res->field_id)", ['class' => 'panel-title']) ?>
        <?= Html::endTag('div') ?>
        <?= Html::beginTag('div', ['class' => 'panel-body']) ?>
        <?= $form->field($resources, 'resources['.$i.'][field_id]')->hiddenInput(['value' => $res->field_id])->label(false) ?>
        <?= $form->field($resources, 'resources['.$i.'][field_name]')->hiddenInput(['value' => $res->field_name])->label(false)?>
        <?= $form->field($resources, 'resources['.$i.'][content]')->textarea([
                'value' => isset($res->content) ? iconv(mb_detect_encoding($res->content), 'UTF-8', $res->content) : '',
                'placeholder' => '["content1", "content2"]'])->label('Content')?>
        <?= $form->field($resources, 'resources['.$i.'][images]')->textarea(['value' => isset($res->images) ? $res->images : '',
                'placeholder' => '["http://urltoimage1.jpg", "http://urltoimage2.jpg"]'])->label('Images')?>
        <?= $form->field($resources, 'resources['.$i.'][comment]')->textInput(['placeholder' => 'Комментарий'])->label('Comment') ?>
        <?= Html::endTag('div') ?>
        <?= Html::endTag('div') ?>
    <?php endfor;?>
    <div class="form-group">
        <?php $condition = strpos(Yii::$app->controller->route,'complete') > 0; ?>
        <?= Html::submitButton( $condition ? 'Complete' : 'Update', ['class' =>'btn btn-success']) ?>
        <?php if ($condition) {
            $walkersArray = \app\modules\api\models\walker\Walker::getFreeWalkersWithId($task->level);
            echo Html::dropDownList('walkerId', null, $walkersArray);
        } ?>
</div>
    <?php ActiveForm::end(); ?>

