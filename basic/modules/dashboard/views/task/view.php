<?php

use app\modules\api\models\constants\level\Level;
use app\modules\api\models\constants\status\Status;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\api\models\task\Task */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Back', 'index', ['class' => 'btn btn-default']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'level',
                'headerOptions' => ['width' => '150'],
                'value' => function($data) {
                    $value = $data->level;
                    return Level::getLabelByLevel($value);
                }],
            [
                'attribute' => 'status',
                'headerOptions' => ['width' => '150'],
                'filter' => Status::getList(),
                'format' => 'raw',
                'value' => function($data) {
                    $value = $data->status;
                    switch ($value) {
                        case Status::AVAILABLE:
                            $class = 'success';
                            $label = Status::getLabelByStatus($value);
                            break;
                        case Status::PERFORMED:
                            $class = 'warning';
                            $label = Status::getLabelByStatus($value);
                            break;
                        case Status::COMPLETED:
                            $class = 'primary';
                            $label = Status::getLabelByStatus($value);
                            break;
                        case Status::DELETED:
                            $class = 'default';
                            $label = Status::getLabelByStatus($value);
                            break;
                        case Status::MODERATED:
                            $class = 'info';
                            $label = Status::getLabelByStatus($value);
                            break;
                        case Status::REJECTED:
                            $class = 'danger';
                            $label = Status::getLabelByStatus($value);
                            break;
                        default:
                            $class = 'important';
                            $label = 'Не задан';
                    }
                    $label = $value == Status::DELETED ? '<del>' . Html::encode($label) . '</del>' : Html::encode($label);
                    $html = Html::tag('span', $label, ['class' => 'label label-' . $class]);
                    if (!in_array($value, [Status::DELETED, Status::AVAILABLE])) {
                        if ($value == Status::PERFORMED) {
                            $html .= '&nbsp; Исполнитель : ';

                        } else if ($value == Status::COMPLETED || Status::MODERATED || Status::REJECTED) {
                            $html .= '&nbsp; Выполнил : ';
                        }

                        $html .= Html::a($data->walker->name, Yii::$app->urlManager->createUrl(
                            'dashboard/walker/view/?id=' . $data->walker->id));
                    }
                    
                    return $html;
                }],
            'walker_id',
            'title',
            [
                'attribute' => 'info',
                'format' => 'html'
            ],
            [
                    'attribute' => 'link',
                    'format' => 'html',
                    'value' => function($data) {
                        return Html::a($data->link, $data->link);
                    }
            ],
            'address',
            'latitude',
            'longitude',
            'cost',
            [
                'attribute' => 'date',
                'format' => ['date', 'php:Y-m-d'],
            ],
            [
                'attribute' => 'release_date',
                'format' => ['date', 'php:Y-m-d'],
            ],
            [
                'attribute' => 'delete_date',
                'format' => ['date', 'php:Y-m-d'],
            ],
            'time'
        ],
    ]) ?>

</div>
