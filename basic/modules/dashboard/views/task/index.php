<?php

use app\modules\api\models\constants\status\Status;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel \app\modules\dashboard\models\task\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model \app\modules\api\models\task\Task */


$this->title = 'Задачи';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if( Yii::$app->session->hasFlash('success') ): ?>
    <div class="alert alert-success alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <?php
        $success = Yii::$app->session->getFlash('success');
        echo is_array($success) ? $success[0] : $success;
        ?>
    </div>
<?php endif;?>
<div class="task-index">

    <?php echo $this->render('icon-description')?>
    <p>
        <?= Html::a('Новая задача', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'status',
                'headerOptions' => ['width' => '150'],
                'filter' => Status::getList(),
                'format' => 'html',
                'value' => function($data) {
                $value = $data->status;
                switch ($value) {
                    case Status::AVAILABLE:
                        $class = 'success';
                        $label = Status::getLabelByStatus($value);
                        break;
                    case Status::PERFORMED:
                        $class = 'warning';
                        $label = Status::getLabelByStatus($value);
                        break;
                    case Status::COMPLETED:
                        $class = 'primary';
                        $label = Status::getLabelByStatus($value);
                        break;
                    case Status::DELETED:
                        $class = 'default';
                        $label = Status::getLabelByStatus($value);
                        break;
                    case Status::MODERATED:
                        $class = 'info';
                        $label = Status::getLabelByStatus($value);
                        break;
                    case Status::REJECTED:
                        $class = 'danger';
                        $label = Status::getLabelByStatus($value);
                        break;
                    default:
                        $class = 'important';
                        $label = 'Не задан';
                }
                $label = $value == Status::DELETED ? '<del>' . Html::encode($label) . '</del>' : Html::encode($label);
                $html = Html::tag('span', $label, ['class' => 'label label-' . $class]);
                return $html;
                }
            ],
            'title',
             'address',
//             'cost',
            [
                'attribute' => 'date',
                'label' => 'Create At',
                'format' => ['date', 'php:Y-m-d h:i:s'],
                'headerOptions' => ['width' => '170'],
            ],
//             'time',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {map} {resource} {edit-resources} {complete}',
                'header' => 'Actions',
                'buttons' => [
                   'map' => function($url, $model) {
                       $var = in_array($model['status'], [Status::COMPLETED, Status::MODERATED, Status::REJECTED]);
                       $mapUrl = Yii::$app->getUrlManager()->createUrl('dashboard/map/' . $model['id']);
                       $color = $var ? '' : 'grey';
                       return Html::a('', $var ? $mapUrl : '#!' , [
                           'class' => 'glyphicon glyphicon-road',
                           'title' => Yii::t('yii', 'Карта'),
                           'data-pjax' => '0',
                           'style' => "color:" . $color
                       ]);
                   },
                    'resource' => function($url, $model) {
                        $var = sizeof($model->taskResources) > 0;
                        $mapUrl = Yii::$app->getUrlManager()->createUrl('dashboard/task/resources/?taskId=' . $model['id']);
                        $color = $var ? '' : 'grey';
                        return Html::a('', $var ? $mapUrl : '#!' , [
                            'class' => 'glyphicon glyphicon-paperclip',
                            'title' => Yii::t('yii', 'Ресурсы'),
                            'data-pjax' => '0',
                            'style' => "color:" . $color
                        ]);
                    },
                    'edit-resources' => function($url, $model) {
                        $var = $model->status != Status::COMPLETED;
                        $mapUrl = Yii::$app->getUrlManager()->createUrl('dashboard/task/edit-resources/?taskId=' . $model['id']);
                        $color = $var ? '' : 'grey';
                        return Html::a('', $var ? $mapUrl : '#!', [
                            'class' => 'glyphicon glyphicon-edit',
                            'title' => Yii::t('yii', 'Редактировать ресурсы '. ($var ? $model->status == Status::PERFORMED ? 'Ходока' : 'Задачи' : '')),
                            'data-pjax' => '0',
                            'style' => "color:" . $color
                        ]);
                    },
                    'complete' => function($url, $model) {
                        /** @var \app\modules\api\models\task\Task $model */
                        $var = $model->isAvailable();
                        $mapUrl = Yii::$app->getUrlManager()->createUrl('dashboard/task/complete/?taskId=' . $model['id']);;
                        return Html::a('', $var ? $mapUrl : '#!' , [
                            'class' => 'glyphicon glyphicon-user',
                            'title' => Yii::t('yii', ($var ? 'Выбрать ходока и завершить задачу' : '')),
                            'data-pjax' => '0',
                            'style' => "color:" . ($var? '' : 'grey')
                        ]);
                    }
                ],
                'headerOptions' => ['width' => '140']
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?>
</div>
