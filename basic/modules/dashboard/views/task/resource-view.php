<?php

/* @var $this yii\web\View */
/* @var $task app\modules\api\models\task\Task */
/* @var $walkerId int|null */

$this->title = $task->title;
$this->params['breadcrumbs'][] = ['label' => 'Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $task->title, 'url' => ['view?id=' . $task->id]];
$this->params['breadcrumbs'][] = 'Ресурсы' . ($walkerId ? ', присланные ходоком' : ' задачи');

if(!isset($resources)) {
   echo  '<div class="alert alert-danger">Нет ресурсов</div>';
    return;
}
?>
<div class="row">
<?php foreach ($resources as $res):
    $_cont = \app\modules\api\models\task\TaskResource::getContentForView($res);
    $hasContent = sizeof($_cont) > 0 && $_cont[0];
    $hasImages = is_array($res['images']) && count($res['images']) > 0 && $res['images'][0] != "";
    ?>
    <div class="panel panel-primary">
        <div class="panel-heading"><?=$res['field_name']?></div>
            <div class="panel-body">
                <div class="panel panel-<?= $hasContent ? 'info' : 'danger'?>">
                    <div class="panel-heading">Content</div>
                        <?php if($hasContent) { ?>
                            <ul class="list-group">
                                <?php foreach ($_cont as $cont): ?>
                                    <li class="list-group-item"><?=$cont?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php } ?>
                </div>
            <div class="panel panel-<?= $hasImages? 'info' : 'danger'?>">
                <div class="panel-heading">Images</div>
                    <?php if($hasImages) { ?>
                        <ul class="list-group">
                            <?php foreach ($res['images'] as $img): ?>
                                 <li class="list-group-item"><img class="img-rounded" src="<?=$img?>" width="240"/></li>
                            <?php endforeach; ?>
                        </ul>
                    <?php } ?>
            </div>
            <div class="panel panel-<?= (isset($res['comment']) ? 'info' : 'danger') ?>">
                <div class="panel-heading">Comment</div>
                    <?php if(isset($res['comment'])) { ?>
                        <ul class="list-group"><li class="list-group-item"><?= $res['comment'] ?></li></ul>
                    <?php }?>
                </div>
            </div>
    </div>
    <br/>
<?php endforeach; ?>
</div>
