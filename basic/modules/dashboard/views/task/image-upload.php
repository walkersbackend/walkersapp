<?php

/**
 * @var string $url
 */

?>
<div class="container-fluid" style="position: fixed; top: inherit; right: 2px; width: 400px">
    <div class="row">
        <div class="thumbnail" style="width: 100%">
            <a href="#!" data-toggle="modal" data-target="#myModal">
                <img class="img-rounded" id="imagePreview" alt="" src="" width="240px" style="display: none; ">
            </a>
            <div class="caption text-center">
                <form role="form" id="uploadForm">
                    <div class="form-group">
                        <input type="file" id="uploadFile" style="margin: 0 auto;">
                        <p class="help-block" id="previewText">Загрузите изобржение</p>
                        <button type="submit" class="btn btn-default">Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 50%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Детали загруженного изображения</h4>
            </div>
            <div class="modal-body">
                    <img class="img-rounded" id="imagePreviewModal" alt="" src="" width="100%">
                <hr>
                <p class="help-block text-center" id="previewTextModal">Загрузите изобржение</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div>
    </div>
</div>

<?php

$js = <<<JS
    $('#uploadForm').on('submit', function (event) {
      event.preventDefault();
      var data = new FormData;
      data.append('file', $('input[type=file]')[0].files[0]);
      $.ajax({
        url: "$url",
        type: "POST",
        data: data,
        crossDomain: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        enctype: 'multipart/form-data',
        success: function(data) {
          if(data.success) {
            var img = $('#imagePreview');
            img.attr('src', data.data.id);
            img.show();
            $('#previewText').text(data.data.id);
          }
        }
      });
   });
   $('#imagePreview').click(function() {
    $('#imagePreviewModal').attr('src', $(this).attr('src'));
    $('#previewTextModal').text($('#previewText').text());
   });
JS;
$this->registerJs($js, $this::POS_READY);

?>
