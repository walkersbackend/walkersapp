<?php

use yii\helpers\Html;

/**
 * @param string $iconType
 * @param string $color
 * @param string $text
 * @return string
 */
function getIcon($iconType, $color = '', $text = '')
{
    return Html::a($text, '#!' , [
        'class' => 'glyphicon glyphicon-' . $iconType,
        'data-pjax' => '0',
        'style' => "color:" . $color
    ]);
}
?>
<div class="panel panel-info">
    <!-- Default panel contents -->
    <div class="panel-heading">Описание иконок</div>
    <div class="panel-body">
        <p>
            <?php
                echo getIcon('eye-open', 'grey') . '&nbsp;';
                echo getIcon('pencil', 'grey') . '&nbsp;';
                echo getIcon('trash', 'grey');
            ?>
            ... - не активные
        </p>
        <p>
            <?php
                echo getIcon('road') . '&nbsp;';
                echo getIcon('paperclip') . '&nbsp;';
                echo getIcon('edit');
            ?>
            ... - активные
        </p>
        <p>
            <?php echo getIcon('eye-open'); ?>
            - просмотр основной информации
        </p>
        <p>
            <?php echo getIcon('pencil'); ?>
            - редактирование информации о задаче
        </p>
        <p>
            <?php echo getIcon('trash'); ?>
            - удаление задачи <span class="label label-warning">при удалении завершенной либо выполняемой задачи, вся инофрмация по ней стирается</span>
        </p>
        <p>
            <?php echo getIcon('road'); ?>
            - просмотр передвижения ходока по карте во время выполнения задачи
        </p>
        <p>
            <?php echo getIcon('paperclip'); ?>
            - просмотр ресурсов задачи либо присланных ходоком, если задача находится на выполнении
        </p>
        <p>
            <?php echo getIcon('edit'); ?>
            - редактирование ресурсов задачи либо присланных ходоком, если задача находится на выполнении. <span class="label label-warning">Ресурсы завершенной задачи не редактируются</span>
        </p>
        <p>
            <?php echo getIcon('user'); ?>
            - назначение ходока на задачу и одновременное ее закрытие с возможностью редкатирования ресурсов
        </p>
        <p>
            <?php
                $label = '<del>' . Html::encode('Удалена') . '</del>';
                $html = Html::tag('span', $label, ['class' => 'label label-default']);
                $href = \Yii::$app->urlManager->createUrl('/swagger/project.html');
                echo $html . ' - задача была удалена через ' . Html::a('Api Project', $href);
            ?>
        </p>
    </div>
</div>
