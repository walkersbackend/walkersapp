<?php

use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

$this->title = 'Статистика ходоков';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="statistics-index">
<?php Pjax::begin(); ?>    <?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'name',
            'format' => 'raw',
            'value' => function($data) {
                return Html::a($data['name'], Yii::$app->urlManager->createUrl('dashboard/walker/view/?id=' . $data['id']));
            }
        ],
        [
            'attribute' => 'startTime',
            'label' => 'Выполнил первую задачу',
            'format' => ['date', 'php:Y-m-d H:i'],
        ],
        [
            'attribute' => 'speed',
            'label' => 'Задач в день'
        ],
        [
            'attribute' => 'doneTasks',
            'label' => 'Кол-во за неделю'
        ]
    ],
]); ?>
<?php Pjax::end(); ?></div>