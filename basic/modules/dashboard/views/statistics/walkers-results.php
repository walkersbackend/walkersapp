<?php

use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var array $walkers
 * @var array $days
 * @var \yii\data\ArrayDataProvider $dataProvider
 */

$this->title = 'Объемы выполнения';
$this->params['breadcrumbs'][] = $this->title;

list($start, $end) = $days;

?>
<div class="statistics-index">
    <?php Pjax::begin(); ?>
    <?= renderDatePicker($start, $end) . '<br/>' ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterSelector' => "[name='from'], [name='to']",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'name',
                'label' => 'ФИО',
                'format' => 'raw',
                'value' => function($data) {
                    return Html::a($data['name'], Yii::$app->urlManager->createUrl('dashboard/walker/view/?id=' . $data['id']));
                }
            ],
            [
                'attribute' => 'amount',
                'label' => 'Количество',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
<?php

function renderDatePicker($start, $end) {
    return DatePicker::widget([
        'name' => 'from',
        'value' => $start,
        'type' => DatePicker::TYPE_RANGE,
        'name2' => 'to',
        'value2' => $end,
        'pluginOptions' => [
            'autoclose'=> true,
            'format' => 'yyyy-mm-dd',
            'language' => 'ru',
            'endDate' => date('Y-m-d'),
        ]
    ]);
}
?>