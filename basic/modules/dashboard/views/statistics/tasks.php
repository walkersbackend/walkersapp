<?php

use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\widgets\Pjax;

/**
 * @var \yii\data\ArrayDataProvider $dataProvider
 * @var \app\modules\dashboard\models\task\TaskStatisticsSearch $searchModel
 */

$this->title = 'Статистика задач';
$this->params['breadcrumbs'][] = $this->title;

list($start, $end) = getStartAndEndDate($dataProvider);

?>
<div class="statistics-index">
    <?php Pjax::begin(); ?>    <?= GridView::widget([
        'filterModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'day',
                'label' => 'День',
                'filter' => DatePicker::widget([
                    'name' => 'from',
                    'value' => $start,
                    'type' => DatePicker::TYPE_RANGE,
                    'name2' => 'to',
                    'value2' => $end,
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=> true,
                        'format' => 'yyyy-mm-dd',
                        'endDate' => date('Y-m-d'),
                        'language' => 'ru'
                    ],
                ])
            ],
            [
                'attribute' => 'open',
                'header' => renderLabelWithIcon(
                    '&nbsp;Доступно',
                    'info',
                    'eye-open'
                ),
            ],
            [
                'attribute' => 'new',
                'header' => renderLabelWithIcon(
                    '&nbsp;Новых',
                    'primary',
                    'plus'
                ),
            ],
            [
                'attribute' => 'close',
                'header' => renderLabelWithIcon(
                    '&nbsp;Завершенно',
                    'success',
                    'ok'
                ),
            ],
            [
                'attribute' => 'remove',
                'header' => renderLabelWithIcon(
                    '&nbsp;Удалено',
                    'default',
                    'trash'
                ),
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?></div>
<?php

/**
 * @param \yii\data\ArrayDataProvider $provider
 * @return array
 */
function getStartAndEndDate($provider) {
    $count = $provider->getTotalCount();
    $end = $provider->allModels[$count - 1]['day'];
    $start = $provider->allModels[0]['day'];
    return [
        $start,
        $end
    ];
}

/**
 * Render label with icon
 * @param string $content
 * @param string $label
 * @param $icon
 * @return string
 */
function renderLabelWithIcon($content, $label = 'default', $icon) {
    return \yii\helpers\Html::label(
        \yii\helpers\Html::tag(
            'span',
            $content,
            ['class' => 'glyphicon glyphicon-' . $icon]
        ), '', ['class' => 'label label-' . $label]
    );
}
?>