<?php

namespace app\modules\dashboard\models\task;

use app\modules\api\models\task\TaskResource;
use yii\base\Model;

class TaskResources extends Model
{
    /**
     * @var array
     */
    public $resources;

    public function search($condition)
    {
        $this->resources = TaskResource::find()->where($condition)->all();
        return $this;
    }

    public function load($data, $formName = null)
    {
        foreach ($data as $res) {
            $_res = new TaskResource();
            $_res->load($res, '');
            $this->resources[] = $_res;
        }
    }

    public function getCount()
    {
        return count($this->resources);
    }
}