<?php

namespace app\modules\dashboard\models\task;

use yii\base\Model;

/**
 * Class TaskStatistics
 * @package app\modules\dashboard\models
 */
class TaskStatistics extends Model
{
    /**
     * @var string
     */
    public $from;

    /**
     * @var integer
     */
    public $to;

    public function rules()
    {
        return [
            [['from', 'to'], 'required'],
            [['from', 'to'], 'date', 'format' => 'Y-m-d'],
        ];
    }
}