<?php

namespace app\modules\dashboard\models\task;

use app\modules\dashboard\services\base\StatisticsServicesExecutor;
use app\modules\dashboard\services\task\statistics\TaskStatisticsService;
use yii\data\ArrayDataProvider;
use yii\data\Sort;

class TaskStatisticsSearch
{
    /**
     * @var StatisticsServicesExecutor
     */
    private $executor;

    /**
     * TaskStatisticsSearch constructor.
     * @param StatisticsServicesExecutor $executor
     * @internal param TaskStatisticsService $service
     */
    public function __construct(StatisticsServicesExecutor $executor)
    {
        $this->executor = $executor;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ArrayDataProvider
     */
    public function search($params)
    {
        if (!isset($params['from']) || !isset($params['to'])) {
            $from = new \DateTime();
            $from->modify('-1 month');
            $to = new \DateTime();
            $params = [
                $from->format('Y-m-d'),
                $to->format('Y-m-d')
            ];
        }

        $allModels = $this->executor->execute(new TaskStatisticsService(...array_values($params)));

        $sort = new Sort([
            'attributes' => [
                'day',
//                'open',
//                'new',
//                'close'
            ],
            'defaultOrder' => [
                'day' => SORT_DESC
            ]
        ]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'sort' => $sort
        ]);

        return $dataProvider;
    }
}