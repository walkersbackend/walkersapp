<?php

namespace app\modules\dashboard\models\walker;

use app\modules\dashboard\services\base\StatisticsServicesExecutor;
use app\modules\dashboard\services\walker\period_activity\WalkersPeriodActivityService;
use yii\data\ArrayDataProvider;
use yii\data\Sort;

class WalkerPeriodActivitySearch
{
    /**
     * @var StatisticsServicesExecutor
     */
    private $executor;

    /**
     * WalkerPeriodActivitySearch constructor.
     * @param StatisticsServicesExecutor $executor
     * @internal param WalkerPeriodActivitySearch $service
     */
    public function __construct(StatisticsServicesExecutor $executor)
    {
        $this->executor = $executor;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return array
     */
    public function search($params)
    {
        if (!isset($params['from']) || !isset($params['to'])) {
            $day = new \DateTime();
            if (intval($day->format('d')) <= 20) {
                $params = [
                    $day->modify('first day of last month')->format('Y-m-d'),
                    $day->modify('last day of this month')->format('Y-m-d')
                ];
            } else {
                $params = [
                    $day->modify('first day of this month')->format('Y-m-d'),
                    date('Y-m-d')
                ];
            }
        }

        $allModels = $this->executor->execute(new WalkersPeriodActivityService(...array_values($params)));

        $sort = new Sort([
            'attributes' => [
                'amount',
            ],
            'defaultOrder' => [
                'amount' => SORT_DESC
            ]
        ]);

        $dataProvider = new ArrayDataProvider([
            'allModels' => $allModels,
            'sort' => $sort
        ]);

        return [
            'provider' => $dataProvider,
            'days' => array_values($params)
        ];
    }
}