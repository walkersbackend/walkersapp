<?php

namespace app\modules\dashboard\controllers;

use Yii;
use yii\web\NotFoundHttpException;

class SwaggerController extends DashboardAccessController
{
    public function actionIndex($path) {
        $paths = [
            'client' => '/swagger\/',
            'project' => '/swagger/project.html',
            'editor' => '/swagger-editor\/',
        ];

        if(isset($paths[$path])) {
            $this->redirect(Yii::$app->getUrlManager()->createUrl($paths[$path]));
        } else {
            throw new NotFoundHttpException();
        }
    }
}