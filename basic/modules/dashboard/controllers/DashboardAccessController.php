<?php

namespace app\modules\dashboard\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

/**
 * Class DashboardAccessController
 * @package app\modules\dashboard\controllers
 */
class DashboardAccessController extends Controller
{
    /**
     * @param \yii\base\Action $action
     * @param bool $isPublic
     * @return bool
     * @throws ForbiddenHttpException
     */
    public function beforeAction($action, $isPublic = false)
    {
        if (Yii::$app->user->isGuest && !$isPublic) {
            throw new ForbiddenHttpException('Доступ запрещен, вам необходимо залогиниться');
        }

        return parent::beforeAction($action);
    }
}