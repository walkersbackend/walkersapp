<?php

namespace app\modules\dashboard\controllers;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;

/**
 * Class MapController
 * @package app\modules\dashboard\controllers
 */
class MapController extends DashboardAccessController
{
    /**
     * @param int $id
     * @return string|\yii\web\Response
     */
    public function actionIndex($id = 0)
    {
        $model = History::findOne([
            'task_id' => $id,
            'result' => [
                Result::COMPLETED,
                Result::REJECTED,
                Result::MODERATED
            ]

        ]);
        $this->layout = 'main';
        if($model) {
            return $this->render('index', ['model' => $model]);
        } else return $this->redirect(\Yii::$app->urlManager->createUrl('/dashboard/task'));
    }

    /**
     * @return string
     */
    public function actionActive()
    {
        $this->layout = 'main';
        return $this->render('map', [
            'tasks' => Task::getActiveTasks(),
            'histories' => History::getActiveHistories()
        ]);
    }

    /**
     * @param \yii\base\Action $action
     * @param bool $isPublic
     * @return bool
     */
    public function beforeAction($action, $isPublic = false)
    {
        if($this->action->actionMethod == 'actionIndex') {
            $isPublic = true;
        }

        return parent::beforeAction($action, $isPublic);
    }
}