<?php

namespace app\modules\dashboard\controllers;

use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\models\task\TaskResource;
use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\task\TaskNotFoundException;
use app\modules\api\modules\client\exceptions\walker\WalkerAlreadyPerformException;
use app\modules\api\modules\client\services\task\assign\AvailableTaskAssigner;
use app\modules\api\modules\client\services\task\complete\TaskCompletionService;
use app\modules\api\modules\client\services\upload\resource\ResourceValidatorService;
use app\modules\dashboard\models\task\TaskResources;
use app\modules\dashboard\models\task\TaskSearch;
use Yii;
use yii\filters\VerbFilter;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends DashboardAccessController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = (new Task())->setDefaultFields();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['task/']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['task/']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a TaskNotFound exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws TaskNotFoundException
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new TaskNotFoundException;
        }
    }

    /**
     * @param $taskId
     * @return string
     */
    public function actionResources($taskId)
    {
        $task = $this->findModel($taskId);
        $walkerId = null;
        if (!$task->isAvailable() && !$task->isDeleted()) {
            $history = History::getPerformingHistory($taskId, $task->status);
            $walkerId = $history ? $history->walker_id : null;
        }
        return $this->render('resource-view', [
            'task' => $task,
            'resources' => TaskResource::getResourcesForTask($taskId, $walkerId),
            'walkerId' => $walkerId
        ]);
    }

    /**
     * @param $taskId
     * @param null/string $role
     * @return string
     */
    public function actionEditResources($taskId, $walkerId = null)
    {
        $task = $this->findModel($taskId);
        if (!$task->isAvailable() && !$task->isDeleted()) {
            $history = History::getPerformingHistory($taskId, $task->status);
            $walkerId = $history ? $history->walker_id : null;
        }
        if (Yii::$app->request->isPost) {
            $resources = Yii::$app->request->post()['TaskResources'];
            try {
                TaskResource::removeAndAddResources(
                    (new ResourceValidatorService())
                        ->setSkipInvalidResources(false)
                        ->validate($resources),
                    $taskId,
                    $walkerId
                );
                Yii::$app->session->addFlash('success', 'Ресурсы успешно обновлены');
                return $this->redirect(['index']);
            } catch (MyException $exception) {
                $taskResources = new TaskResources();
                $taskResources->load($resources['resources']);
                Yii::$app->session->addFlash('error', $exception->getErrorBody()['message']);
            }
        } else {
            $taskResources = (new TaskResources())->search(['task_id' => $taskId, 'walker_id' => $walkerId]);
        }
        return $this->render('resource-edit', [
            'task' => $task,
            'resources' => $taskResources,
            'walkerId' => $walkerId
        ]);
    }

    /**
     * @param int $taskId
     * @param null|int $walkerId
     * @return string|\yii\web\Response
     */
    public function actionComplete($taskId, $walkerId = null)
    {
        if (Yii::$app->request->isPost) {
            $resources = Yii::$app->request->post('TaskResources');
            $walkerId = Yii::$app->request->post('walkerId');
            try {
                $task = $this->findModel($taskId);
                $walker = Walker::findOne(['id' => $walkerId]);
                if (History::getCurrentHistory($walker)) {
                    throw new WalkerAlreadyPerformException;
                }
                if ($data = (new ResourceValidatorService())->setSkipInvalidResources(false)->validate($resources)) {
                    (new AvailableTaskAssigner($walker, $task))->assign();
                    TaskResource::removeAndAddResources($data, $taskId, $walkerId);
                    (new TaskCompletionService())->complete($walker);
                    Yii::$app->session->setFlash('success', 'Задача успешно закрыта');
                    return $this->redirect(['index']);
                }
            } catch (MyException $exception) {
                Yii::$app->session->setFlash('error', $exception->getErrorBody()['message']);
                $taskResources = new TaskResources();
                $taskResources->load($resources['resources']);
                return $this->render('resource-edit', [
                    'task' => $task,
                    'resources' => $taskResources,
                    'walkerId' => $walker->id
                ]);
            }
        }

        return $this->actionEditResources($taskId, $walkerId);
    }
}
