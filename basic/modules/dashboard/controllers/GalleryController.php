<?php

namespace app\modules\dashboard\controllers;

use Yii;
use yii\web\Controller;

/**
 * Class GalleryController
 * @package app\modules\dashboard\controllers
 */
class GalleryController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $files = scandir(Yii::getAlias('@uploads') . '/');
        for ($i = 0; $i < count($files); ++$i) {
            if (!preg_match('/.(jpg|jpeg)/', $files[$i])) {
                array_splice($files, $i, 1);
                $i--;
            }
        }
        return $this->render('index', ['images' => $files]);
    }
}