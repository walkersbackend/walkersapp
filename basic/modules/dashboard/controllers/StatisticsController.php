<?php

namespace app\modules\dashboard\controllers;

use app\modules\dashboard\models\task\TaskStatisticsSearch;
use app\modules\dashboard\models\walker\WalkerPeriodActivitySearch;
use app\modules\dashboard\services\base\StatisticsServicesExecutor;
use app\modules\dashboard\services\walker\statistics\WalkerStatisticsService;
use yii\data\ArrayDataProvider;

/**
 * Class StatisticsController
 * @package app\modules\dashboard\controllers
 */
class StatisticsController extends DashboardAccessController
{
    /**
     * @var StatisticsServicesExecutor
     */
    private $executor;

    /**
     * StatisticsController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param StatisticsServicesExecutor $executor
     * @param array $config
     */
    public function __construct($id, $module, StatisticsServicesExecutor $executor, $config = [])
    {
        $this->executor = $executor;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return string
     */
    public function actionWalkers()
    {
        return $this->render('walkers', [
            'dataProvider' => new ArrayDataProvider([
                'allModels' => $this->executor->execute(new WalkerStatisticsService())
            ])
        ]);
    }

    /**
     * @return string
     */
    public function actionTasks()
    {
        $searchModel = new TaskStatisticsSearch($this->executor);
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('tasks', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string
     */
    public function actionWalkersResults()
    {
        $searchModel = new WalkerPeriodActivitySearch($this->executor);
        $result = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render('walkers-results', [
            'searchModel' => $searchModel,
            'dataProvider' => $result['provider'],
            'days' => $result['days']
        ]);
    }
}