<?php

namespace app\modules\dashboard\services\walker\statistics;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\history\History;
use app\modules\api\models\walker\Walker;
use app\modules\dashboard\services\base\DashboardStatistics;

/**
 * Class StatisticsService
 * @package app\modules\api\modules\project\services\statistics
 */
class WalkerStatisticsService extends DashboardStatistics
{
    /**
     * @param Walker|\yii\db\ActiveRecord $walker
     * @return array
     */
    private function getWalkerStat($walker)
    {
        $stat = [];
        $stat['id'] = $walker->id;
        $stat['name'] = $walker->name;
        return array_merge($stat, $this->getWalkerActivity($walker));
    }

    /**
     * @param Walker $walker
     * @return array
     */
    private function getWalkerActivity($walker)
    {
        $keys = ['startTime','speed', 'doneTasks'];

        if (!$historyStartTime = $this->getWalkerFirstTaskTime($walker)) {
            return array_fill_keys($keys, null);
        }

        return array_combine($keys, [
            $historyStartTime,
            $this->getWalkerPerformance($walker),
            $this->getWalkerLastCompletedTasks($walker)
        ]);
    }

    /**
     * @param Walker $walker
     * @return float
     */
    private function getWalkerPerformance($walker)
    {
        $result = Result::COMPLETED;
        $sql = "SELECT SUM(amount) / COUNT(*) as speed FROM (SELECT DISTINCT FROM_UNIXTIME(start_time,\"%Y-%D-%M\") AS date,
        COUNT(FROM_UNIXTIME(start_time,\"%Y-%D-%M\")) AS amount FROM history WHERE walker_id = $walker->id AND result = $result GROUP BY date ) a";
        $row = $this->db->createCommand($sql)->queryAll();
        return floatval($row[0]['speed']);
    }

    private function getWalkerLastCompletedTasks($walker, $days = 7)
    {
        $week = time() - $days * 24 * 3600;
        return count(History::find()->where([
            'AND',
            ['result' => Result::COMPLETED],
            ['walker_id' => $walker->id],
            ['>', 'end_time', $week],
        ])->all());
    }

    /**
     * @param Walker $walker
     * @return int|null
     */
    private function getWalkerFirstTaskTime($walker)
    {
        $historyStart = History::findOne([
            'walker_id' => $walker->id,
            'result' => Result::COMPLETED
        ]);

        return empty($historyStart) ? null : $historyStart->end_time;
    }

    /**
     * @return mixed
     */
    public function getStatistics()
    {
        $walkers = Walker::find()->all();
        $stat = [];
        foreach ($walkers as $walker) {
            $stat[] = $this->getWalkerStat($walker);
        }

        return $stat;
    }
}