<?php

namespace app\modules\dashboard\services\walker\period_activity;

use app\modules\api\models\constants\result\Result;
use app\modules\dashboard\services\base\DashboardStatistics;

/**
 * Class WalkerPeriodActivityService
 * @package app\modules\dashboard\services\walker
 */
class WalkersPeriodActivityService extends DashboardStatistics
{
    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * TaskStatisticsService constructor.
     * @param string $from
     * @param string $to
     */
    public function __construct($from, $to)
    {
        list($this->from, $this->to) = [$from, $to];
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getStatistics()
    {
        return $this->getData();
    }

    private function getData()
    {
        $result = Result::COMPLETED;
        $field = 'end_time';
        $format = "\"%Y-%m-%d\"";
        $sql = "SELECT walker.id, walker.name, t.amount AS amount";
        $sql .= " FROM walker RIGHT JOIN (SELECT DISTINCT w.id as id, w.name as name,";
        $sql .= " COUNT(w.name) AS amount FROM history h, walker w WHERE h.result = {$result}";
        $sql .= " AND {$this->fromUnixTime($field, $format)} BETWEEN '{$this->from}' AND";
        $sql .= " '{$this->to}' AND h.walker_id = w.id GROUP BY w.id) t on t.id = walker.id";
        return $this->db->createCommand($sql)->queryAll();
    }
}