<?php

namespace app\modules\dashboard\services\task\statistics;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\constants\status\Status;
use app\modules\dashboard\services\base\DashboardStatistics;
use DateInterval;
use DatePeriod;

/**
 * Class TaskStatisticsService
 * @package app\modules\dashboard\project\services\task
 */
class TaskStatisticsService extends DashboardStatistics
{
    /**
     * @var array
     */
    private $keys = ['day', 'open', 'new', 'close', 'remove'];

    /**
     * @var string
     */
    private $from;

    /**
     * @var string
     */
    private $to;

    /**
     * TaskStatisticsService constructor.
     * @param string $from
     * @param string $to
     */
    public function __construct($from, $to)
    {
        list($this->from, $this->to) = [$from, $to];
        parent::__construct();
    }

    /**
     * Generates date range interval
     * @param string $from
     * @param string $to
     * @return array
     */
    private function generateDays($from, $to)
    {
        $startDate = new \DateTime($from);
        $endDate = new \DateTime($to);
        $endDate->modify('+1 day');
        $interval = new DateInterval('P1D');
        $period = new DatePeriod($startDate, $interval, $endDate);
        $days = [];

        /* @var \DateTime $datetime */
        foreach($period as $datetime) {
            $arr = array_fill_keys($this->keys, 0);
            $arr['day'] = $datetime->format("Y-m-d");
            $days[] = $arr;
        }
        return $days;
    }

    /**
     * Return array of tasks statistics
     * @return array
     */
    public function getStatistics()
    {
        $format = "\"%Y-%m-%d\"";
        $days = $this->generateDays($this->from, $this->to);

        $closedTasks = $this->getClosedTasks($this->from, $this->to, $format);
        $newTasks = $this->getNewTasks($this->from, $this->to, $format);
        $deletedTask = $this->getDeletedTasks($this->from, $this->to, $format);

        $this->fillDaysArray($days, $newTasks, $closedTasks, $deletedTask);

        $beforeFromStat = $this->getOpenTasksBeforeDate($this->from, $format);
        $days[0]['open'] += $beforeFromStat;

        $this->calculateOpenTasks($days);
        return $days;
    }

    /**
     * Return count of new tasks in date range
     * @param string $from
     * @param string $to
     * @param $format
     * @return array
     */
    private function getNewTasks($from, $to, $format)
    {
        $fromUnixTimeOfDate = $this->fromUnixTime('date', $format);
        $sql = "SELECT DISTINCT {$fromUnixTimeOfDate} AS day, COUNT({$fromUnixTimeOfDate}) AS new";
        $sql .= " FROM task WHERE {$fromUnixTimeOfDate} BETWEEN '{$from}'";
        $sql .= " AND '{$to}' GROUP BY day";
        return \Yii::$app->db->createCommand($sql)->queryAll();
    }

    /**
     * Return count of open tasks in date range
     * @param string $from
     * @param string $to
     * @param $format
     * @return array
     */
    private function getClosedTasks($from, $to, $format)
    {
        $result = Result::COMPLETED;
        $fromUnixTimeOfEndTime = $this->fromUnixTime('end_time', $format);
        $sql = "SELECT DISTINCT {$fromUnixTimeOfEndTime} ";
        $sql .= "AS day, COUNT({$fromUnixTimeOfEndTime}) AS close FROM history WHERE result = $result";
        $sql .= " AND {$fromUnixTimeOfEndTime} BETWEEN '{$from}' AND '{$to}' GROUP BY day";
        return \Yii::$app->db->createCommand($sql)->queryAll();
    }

    /**
     * Return count of deleted tasks in date range
     * @param string $from
     * @param string $to
     * @param $format
     * @return array
     */
    private function getDeletedTasks($from, $to, $format)
    {
        $status = Status::DELETED;
        $fromUnixTimeOfDate = $this->fromUnixTime('delete_date', $format);
        $sql = "SELECT DISTINCT {$fromUnixTimeOfDate} AS day, COUNT({$fromUnixTimeOfDate}) AS remove";
        $sql .= " FROM task WHERE status = {$status} AND {$fromUnixTimeOfDate} BETWEEN '{$from}'";
        $sql .= " AND '{$to}' GROUP BY day";
        return \Yii::$app->db->createCommand($sql)->queryAll();
    }

    /**
     * Run filling days array
     * @param array $days
     * @param array $new
     * @param array $closed
     * @param array $deleted
     */
    private function fillDaysArray(&$days, $new, $closed, $deleted)
    {
        foreach ($days as &$item) {
            $this->fillDay($item['day'], $new, $item);
            $this->fillDay($item['day'], $closed, $item);
            $this->fillDay($item['day'], $deleted, $item);
        }
    }

    /**
     * Fills days array with statistics values
     * @param string $day
     * @param array $array
     * @param array $dayArr
     */
    private function fillDay($day, $array, &$dayArr)
    {
        array_walk($array, function($value) use ($day, &$dayArr) {
            $keys = array_keys($value);
            if ($value[$keys[0]] == $day) {
                $dayArr[$keys[1]] = intval($value[$keys[1]]);
            }
        });
    }

    /**
     * Calculate a open tasks in date range
     * @param array $days
     */
    private function calculateOpenTasks(&$days)
    {
        for($i = 0; $i < count($days); ++$i) {
            $item = &$days[$i];

            $item['open'] += $item['new'] - $item['close'] - $item['remove'];

            if (isset($days[$i - 1])) {
                $item['open'] += $days[$i - 1]['open'];
            }
        }
    }

    /**
     * Calculate a open tasks count before start date, which date range beginning
     * @param string $date
     * @param string $format
     * @return int
     */
    private function getOpenTasksBeforeDate($date, $format) {
        $fromUnixTimeOfDate = $this->fromUnixTime('date', $format);
        $sql = "SELECT COALESCE(SUM(new), 0) AS new FROM (SELECT DISTINCT {$fromUnixTimeOfDate} AS day";
        $sql .= " , COUNT({$fromUnixTimeOfDate}) AS new  FROM task WHERE";
        $sql .= " {$fromUnixTimeOfDate} < '{$date}' GROUP BY day) t1";
        $new = \Yii::$app->db->createCommand($sql)->queryOne()['new'];

        $result = Result::COMPLETED;
        $fromUnixTimeOfEndTime = $this->fromUnixTime('end_time', $format);
        $sql = "SELECT COALESCE(SUM(close), 0) AS close FROM (SELECT DISTINCT {$fromUnixTimeOfEndTime}";
        $sql .= " AS day, COUNT({$fromUnixTimeOfEndTime}) AS close FROM history WHERE result = $result";
        $sql .= " AND {$fromUnixTimeOfEndTime} < '{$date}' GROUP BY day) t2";
        $close =  \Yii::$app->db->createCommand($sql)->queryOne()['close'];

        return $new - $close;
    }

}