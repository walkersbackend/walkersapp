<?php

namespace app\modules\dashboard\services\base;

/**
 * Class StatisticsExecutor
 * @package app\modules\dashboard\services\base
 */
class StatisticsServicesExecutor
{
    /**
     * @param IStatistics $service
     * @return mixed
     */
    public function execute(IStatistics $service)
    {
        return $service->getStatistics();
    }
}