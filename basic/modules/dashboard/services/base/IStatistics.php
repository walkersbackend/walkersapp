<?php

namespace app\modules\dashboard\services\base;

/**
 * Interface IStatistics
 * @package app\modules\dashboard\services\base
 */
interface IStatistics
{
    /**
     * @return mixed
     */
    public function getStatistics();
}