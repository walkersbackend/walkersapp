<?php

namespace app\modules\dashboard\services\base;

/**
 * Class BaseStatistics
 * @package app\modules\dashboard\services\base
 */
abstract class DashboardStatistics implements IStatistics
{
    /**
     * @var \Yii\db\Connection
     */
    protected $db;

    /**
     * BaseStatistics constructor.
     */
    public function __construct()
    {
        $this->db = \Yii::$app->db;
    }

    /**
     * @param string $field
     * @param string $format
     * @return string
     */
    protected function fromUnixTime($field, $format) {
        return "FROM_UNIXTIME($field, $format)";
    }
}