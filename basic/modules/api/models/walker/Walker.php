<?php

namespace app\modules\api\models\walker;

use app\modules\api\models\constants\level\Level;
use app\modules\api\models\definitions\Definitions;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\models\task\TaskResource;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "walker".
 *
 * @property integer $id
 * @property integer $level
 * @property string $name
 * @property string $email
 * @property string $number
 * @property integer $date
 * @property string $token
 * @property string $user_agent
 * @property integer $last_activity
 *
 * @property History[] $histories
 * @property TaskResource[] $taskResources
 * @property Task[] $tasks
 */
class Walker extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'walker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['level', 'name', 'email'], 'required'],
            [['level', 'date', 'last_activity'], 'integer'],
            [['date', 'last_activity'], 'default', 'value' => time()],
            [['name', 'email'], 'string', 'max' => 50],
            [['number'], 'string', 'max' => 255],
            [['number'], 'default', 'value' => ''],
            [['user_agent'], 'default', 'value' => null],
            [['user_agent'], 'string'],
            [['token'], 'string', 'max' => 32],
            [['token'], 'default', 'value' => null],
            [['level'], 'in', 'range' => Level::getLevels()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'name' => 'Name',
            'email' => 'Email',
            'number' => 'Number',
            'date' => 'Date',
            'token' => 'Token',
            'user_agent' => 'User Agent',
            'last_activity' => 'Last Activity'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['walker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskResources()
    {
        return $this->hasMany(TaskResource::className(), ['walker_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['walker_id' => 'id']);
    }

    /**
     * Generate walker's token, create if not exist
     * @return string
     */
    public function generateToken()
    {
        $token = $this->token;
        if (strlen($token) == Definitions::TOKEN_LENGTH) {
            return $token;
        }

        $token = strtolower(md5(uniqid($this->email)));
        while (self::findOne(['token' => $token])) {
            $token = strtolower(md5(uniqid($this->email)));
        }

        $this->token = $token;
        $this->save();
        return $token;
    }

    /**
     * Return assoc array where keys is walkers_id, values is walkers names
     * @param $level
     * @return array
     */
    public static function getFreeWalkersWithId($level)
    {
        $walkers = self::find()->select(['id', 'name'])->where('level >=' . $level)->all();
        $array = [];
        foreach ($walkers as $walker) {
            /** @var Walker $walker */
            if (History::getCurrentHistory($walker)) {
                continue;
            }
            $array[$walker->id] = $walker->name;
        }
        return $array;
    }

    public function saveUserAgent($userAgent)
    {
        if ($this->user_agent == $userAgent) {
            return;
        }

        $this->updateAttributes(['user_agent' => $userAgent]);
    }
}
