<?php

namespace app\modules\api\models\track;

use app\modules\api\models\history\History;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "track".
 *
 * @property integer $id
 * @property integer $history_id
 * @property double $latitude
 * @property double $longitude
 * @property integer $date
 *
 * @property History $history
 */
class Track extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'track';
    }

    /**
     * Upload locations
     * @param array $locations
     * @param History $history
     */
    public static function uploadLocations($locations, $history)
    {
        $start_time = $history->start_time;

        foreach ($locations as $l) {
            $arr = [];
            $arr['history_id'] = $history->id;
            $arr['latitude'] = $l['latitude'];
            $arr['longitude'] = $l['longitude'];
            $arr['date'] = $start_time + $l['time'];
            $model = new self();
            $model->load($arr, '');
            $model->save();
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['history_id', 'latitude', 'longitude', 'date'], 'required'],
            [['history_id', 'date'], 'integer'],
            [['id'], 'default', 'value' => null],
            [['latitude', 'longitude'], 'number'],
            [['history_id'], 'exist', 'skipOnError' => true, 'targetClass' => History::className(), 'targetAttribute' => ['history_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'history_id' => 'History ID',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistory()
    {
        return $this->hasOne(History::className(), ['id' => 'history_id']);
    }
}
