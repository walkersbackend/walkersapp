<?php

namespace app\modules\api\models\definitions;

/**
 * Class of constants
 * @package app\modules\api\models\Definitions
 */
class Definitions
{

    /**
     * Default execution time od a task
     * @const int
     */
    const TASK_EXECUTION_TIME = 3600;

    /**
     * Require time format
     * @const string
     */
    const TIME_FORMAT = "Y-m-dTH:i:sZ";

    /**
     * Require token length
     * @const int
     */
    const TOKEN_LENGTH = 32;

    /**
     * Required email domain
     * @const string
     */
    const REQUIRE_EMAIL_DOMAIN = '@gmail.com';

    /**
     * Return current time with offset
     * @param null|int $offset
     * @return int
     */
    public static function getTime($offset = null)
    {
        return time() + $offset;
    }

    /**
     * Return diff in seconds between current time and given
     * @param int $startTime
     * @param int $fullTime int
     * @return int
     */
    public static function getDateDiffFromCurrent($fullTime ,$startTime)
    {
        return $fullTime - (time() - $startTime);
    }
}