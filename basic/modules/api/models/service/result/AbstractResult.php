<?php

namespace app\modules\api\models\service\result;

use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class AbstractResult
 * @package app\modules\api\models\service\result
 */
abstract class AbstractResult
{
    /**
     * @var int
     */
    const SUCCESS = 1;

    /**
     * @var mixed|null
     */
    protected $identity;

    /**
     * @var int
     */
    protected $result;

    /**
     * @var MyException
     */
    protected $exception;

    /**
     * AbstractResult constructor.
     * @param $result int
     * @param mixed|null $identity
     * @param \Exception $exception
     */
    final public function __construct($result, $identity = null, $exception = null)
    {
        $this->result = $result;
        $this->identity = $identity;
        $this->exception = $exception;
    }

    /**
     * @return bool
     */
    final public function isSuccess()
    {
        return $this->result == self::SUCCESS;
    }

    /**
     * @return mixed|null
     */
    public function getIdentity()
    {
        return $this->identity;
    }

    /**
     * @return MyException|null
     */
    final public function getException()
    {
        return $this->exception;
    }
}