<?php

namespace app\modules\api\models\history;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\constants\status\Status;
use app\modules\api\models\task\Task;
use app\modules\api\models\task\TaskResource;
use app\modules\api\models\track\Track;
use app\modules\api\models\walker\Walker;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "history".
 *
 * @property integer $id
 * @property integer $walker_id
 * @property integer $task_id
 * @property integer $result
 * @property integer $start_time
 * @property integer $end_time
 * @property string $comment
 * @property string $walker_user_agent
 *
 * @property Task $task
 * @property Walker $walker
 * @property Track[] $tracks
 */
class History extends ActiveRecord
{
    /**
     * @var string
     */
    const CURRENT_TASK_TIME_IS_UP = 'Время вышло';

    /**
     * @var string
     */
    const REJECTED_TASK_TIME_IS_UP = 'Задача была возвращена, время на выполнение вышло';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * Update performing tasks, which execution time is up
     * @param int $time
     * @param null|int $walkerId
     */
    public static function updateCurrentTasks($time, $walkerId = null)
    {
        $message = self::CURRENT_TASK_TIME_IS_UP;
        $result = Result::CANCELED;
        $status = Status::AVAILABLE;

        $cond = is_null($walkerId) ? " IS NOT NULL" : " = $walkerId";

        $sql = "UPDATE task t, history h 
        SET t.status = $status, t.walker_id = NULL, h.end_time = $time, 
        h.result = $result, h.comment = '{$message}'
        WHERE t.id = h.task_id AND h.end_time IS NULL AND h.result IS NULL
        AND h.start_time < $time - t.time AND h.walker_id $cond";
        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * Return current history, if start_time less then current time minus task execution time,
     * update history and makes task available then return null
     * @param Walker $walker
     * @return History|null
     */
    public static function getCurrentHistory($walker)
    {
        self::updateCurrentTasks(time(), $walker->id);

        $history =  self::findOne([
            'walker_id' => $walker->id,
            'result' => null,
            'end_time' => null
        ]);

        return $history;
    }

    /**
     * Return result of execute a task
     * @param Task $task
     * @return array|null
     */
    public static function getResult($task)
    {
        $history = self::findOne([
            'task_id' => $task->id,
            'result' => Result::MODERATED
        ]);

        if (!$history) {
            return null;
        }

        $path = $history->getTracks()->select(['latitude', 'longitude', 'date'])->all();

        $duration = $history->end_time - $history->start_time;
        return [
            'walker_id' => $history->walker_id,
            'walker_app_version' => $history->walker_user_agent,
            'duration' => $duration,
            'complete_time' => $history->end_time,
            'resources' => TaskResource::getResourcesForTask($task->id, $history->walker_id),
            'locations' => empty($path) ? null : $path
        ];
    }

    /**
     * Return active histories
     * @return array|ActiveRecord[]
     */
    public static function getActiveHistories()
    {
        return self::find()->where([
            'result' => null,
            'end_time' => null
        ])->all();
    }

    /**
     * Returns a walkers last cancelled task
     * @param Walker $walker
     * @return History|ActiveRecord|null
     */
    public static function getLastCancelledOrCompletedTask($walker)
    {
        $to = time();
        $from = $to - Task::RESERVE_TIME;
        $column = 'end_time';
        $condition = [
            'AND',
            ['walker_id' => $walker->id],
            ['not', [$column => null]],
            ['in', 'result', [Result::MODERATED, Result::CANCELED]],
            ['between', $column, $from, $to]
        ];

        return self::find()->where($condition)->orderBy([$column => SORT_DESC])->limit(1)->one();
    }

    /**
     * Update tasks, which result = `Result::REJECTED` and execution time is up
     * @param int $time
     * @param null|int $walkerId
     */
    public static function updateRejectedTasks($time, $walkerId = null)
    {
        $message = self::REJECTED_TASK_TIME_IS_UP;
        $cancel = Result::CANCELED;
        $available = Status::AVAILABLE;
        $rejectedR = Result::REJECTED;
        $rejectedS = Status::REJECTED;
        $interval = Task::HOLDING_TIME;
        $cond = is_null($walkerId) ? " IS NOT NULL" : " = $walkerId";

        $sql = "UPDATE task t, history h 
        SET t.status = $available, t.walker_id = NULL, h.end_time = $time, 
        h.result = $cancel, h.comment = '{$message}'
        WHERE t.id = h.task_id AND t.status = $rejectedS
        AND h.end_time IS NULL AND h.result = $rejectedR
        AND h.start_time < $time - $interval AND h.walker_id $cond";

        \Yii::$app->db->createCommand($sql)->execute();
    }

    /**
     * Return a rejected history by walker_id and task_id
     * @param int $walkerId
     * @param int $taskId
     * @return History|null
     */
    public static function getRejectedHistory($walkerId, $taskId)
    {
        self::updateRejectedTasks(time(), $walkerId);

        $condition = [
            'walker_id' => $walkerId,
            'task_id' => $taskId,
            'result' => Result::REJECTED
        ];

        return self::findOne($condition);
    }

    /**
     * @param $walkerId
     * @return null|History[]
     */
    public static function getRejectedTasks($walkerId)
    {
        $condition = [
            'walker_id' => $walkerId,
            'result' => Result::REJECTED
        ];

        return self::find()->where($condition)->all();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['walker_id', 'task_id', 'start_time'], 'required'],
            [['walker_id', 'task_id', 'start_time'], 'integer'],
            [['comment'], 'string', 'max' => 255],
            [['result', 'end_time'], 'default', 'value' => null],
            [['walker_user_agent'], 'string'],
            [['walker_user_agent'], 'default', 'value' => null],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['walker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Walker::className(), 'targetAttribute' => ['walker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'walker_id' => 'Walker ID',
            'task_id' => 'Task ID',
            'result' => 'Result',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'comment' => 'Comment',
            'walker_user_agent' => 'Walker User Agent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalker()
    {
        return $this->hasOne(Walker::className(), ['id' => 'walker_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTracks()
    {
        return $this->hasMany(Track::className(), ['history_id' => 'id']);
    }

    /**
     * History constructor.
     * Create new history and save
     * @param array $config
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->save();
    }

    /**
     * Change result field
     * @param int $result
     */
    public function changeResult($result)
    {
        $this->result = $result;
        $this->save(false);
    }

    /**
     * @param int $taskId
     * @param string|null $result
     * @return History|null
     */
    public static function getPerformingHistory($taskId, $result = null)
    {
//        $condition = [
//            'task_id' => $taskId,
//            'result'  => $result
//        ];
//
//        if (!isset($result) || $result == Result::REJECTED) {
//            $condition['end_time'] = null;
//        }

        $condition = self::getConditionByTaskStatus($result);
        $condition['task_id'] = $taskId;

        return self::findone($condition);
    }

    private static function getConditionByTaskStatus($status)
    {
        $condition = [];

        switch ($status) {
            case Status::COMPLETED:
                $condition['result'] = Result::COMPLETED;
                break;
            case Status::REJECTED:
                $condition['result'] = Result::REJECTED;
                break;
            case Status::MODERATED:
                $condition['result'] = Result::MODERATED;
                break;
            case Status::PERFORMED:
                $condition['result'] = null;
                $condition['end_time'] = null;
                break;
        }

        return $condition;
    }
}
