<?php

namespace app\modules\api\models\response;

/**
 * Class for forming the response body
 * @package app\modules\api\models\ResponseBody
 */
class ResponseBody
{
    /**
     * @var bool
     */
    private $success;

    /**
     * @var array|null
     */
    private $error;

    /**
     * @var array|null
     */
    private $data;

    /**
     * ResponseBody constructor.
     */
    function __construct()
    {
        $this->success = false;
        $this->error = null;
        $this->data = null;
    }

    /**
     * Set success
     * @param bool $success
     * @return ResponseBody
     */
    public function setSuccess($success)
    {
        $this->success = $success;
        return $this;
    }

    /**
     * Set error
     * @param array $error
     * @return ResponseBody
     */
    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }

    /**
     * Set data
     * @param array $data
     * @return ResponseBody
     */
    public function setData($data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Return response body
     * @return array
     */
    public function getAsArray()
    {
        return [
            'success' => $this->success,
            'error' => $this->error,
            'data' => $this->data
        ];
    }
}