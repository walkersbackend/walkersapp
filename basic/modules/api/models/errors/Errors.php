<?php

namespace app\modules\api\models\errors;

/**
 * Class for errors
 * @package app\modules\api\models\errors
 */
class Errors
{
    /**
     * @const int
     */
    const WALKER_NOT_FOUND_BY_EMAIL = 1;

    /**
     * @const int
     */
    const WALKER_NOT_FOUND_BY_TOKEN = 2;

    /**
     * @const int
     */
    const WALKER_ALREADY_PERFORMING = 3;

    /**
     * @const int Таск не найден.
     */
    const TASK_NOT_FOUND = 4;

    /**
     * @const int
     */
    const TASK_NOT_AVAILABLE = 5;

    /**
     * @const int
     */
    const TASK_NOT_GRANTED = 6;

    /**
     * @const int
     */
    const NOT_FOUND_CURRENT_TASK = 7;

    /**
     * @const int
     */
    const RESOURCE_NOT_LOADED = 8;

    /**
     * @const int
     */
    const LOCATION_NOT_LOADED = 9;

    /**
     * @const int
     */
    const FILE_NOT_LOAD = 10;

    /**
     * @const int
     */
    const TASK_NOT_CREATE = 11;

    /**
     * @const int
     */
    const TASK_NOT_COMPLETED = 12;

    /**
     * @const int
     */
    const NOT_FOUND_LAST_TASK = 13;

    /**
     * @const int
     */
    const WALKER_NOT_FOUND_BY_ID = 14;

    /**
     * @const int
     */
    const TASK_CANNOT_BE_REOPENED = 15;

    /**
     * Return array with code and message
     * @param int $errorCode
     * @return array
     * @throws \Exception
     */
    public static function getBody($errorCode)
    {
        $list = self::getMessageList();
        if (empty($list[$errorCode])) {
            throw new \Exception('Wrong error code');
        }

        return [
            'code' => $errorCode,
            'message' => $list[$errorCode]
        ];
    }

    /**
     * Return array like map of error code and error massage
     * @return array
     */
    private static function getMessageList()
    {
        return [
            self::WALKER_NOT_FOUND_BY_EMAIL => 'Walker with given email is not found',
            self::WALKER_NOT_FOUND_BY_TOKEN => 'Walker with given token is not found',
            self::WALKER_ALREADY_PERFORMING => 'You are already performing a task',
            self::TASK_NOT_FOUND => 'Task with given id is not found',
            self::TASK_NOT_AVAILABLE => 'Task is not available',
            self::TASK_NOT_GRANTED => 'Not enough level',
            self::NOT_FOUND_CURRENT_TASK => 'Not found current task',
            self::RESOURCE_NOT_LOADED => 'Bad resources',
            self::LOCATION_NOT_LOADED => 'Bad locations',
            self::FILE_NOT_LOAD => 'File not load',
            self::TASK_NOT_CREATE => 'Task is not create',
            self::TASK_NOT_COMPLETED => 'Task is not completed',
            self::NOT_FOUND_LAST_TASK => 'Not found last task',
            self::WALKER_NOT_FOUND_BY_ID => 'Walker with given id is not found',
            self::TASK_CANNOT_BE_REOPENED => 'Task cannot be reopened'
        ];
    }

}
