<?php

namespace app\modules\api\models\task;

use app\modules\api\models\constants\level\Level;
use app\modules\api\models\constants\status\Status;
use app\modules\api\models\definitions\Definitions;
use app\modules\api\models\history\History;
use app\modules\api\models\walker\Walker;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $level
 * @property integer $status
 * @property string $title
 * @property string $info
 * @property string $link
 * @property string $address
 * @property double $latitude
 * @property double $longitude
 * @property double $cost
 * @property integer $date
 * @property integer $release_date
 * @property integer $delete_date
 * @property integer $time
 * @property integer $walker_id
 *
 * @property Walker $walker
 * @property History[] $histories
 * @property TaskResource[] $taskResources
 */
class Task extends ActiveRecord
{
    /**
     * The time during which the hold is in reserve
     */
    const RESERVE_TIME = 300;

    /**
     * The time during which the hold is in reserve
     */
    const HOLDING_TIME = 24 * 3600;

    /**
     * Set's default fields to model
     * @return Task
     */
    public function setDefaultFields()
    {
        $this->level = Level::BASIC;
        $this->time = Definitions::TASK_EXECUTION_TIME;
        $this->date = time();
        $this->status = Status::AVAILABLE;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'latitude', 'longitude', 'cost', 'link', 'address'], 'required'],
            [['title', 'link', 'address'], 'string', 'max' => 255],
            [['info'], 'string'],
            [['release_date'], 'safe'],
            [['link'], 'url', 'defaultScheme' => 'http'],
            [['latitude', 'longitude'], 'match', 'pattern' => '/-?\d{1,3}\.\d+/'],
            [['latitude'], 'double', 'min' => -90, 'max' => 90],
            [['latitude'], 'double', 'min' => -180, 'max' => 180],
            [['time'], 'integer', 'min' => 0],
            [['walker_id'], 'integer'],
            [['walker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Walker::className(), 'targetAttribute' => ['walker_id' => 'id']],
            [['cost'], 'double']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'level' => 'Level',
            'status' => 'Status',
            'title' => 'Title',
            'info' => 'Info',
            'link' => 'Link',
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'cost' => 'Cost',
            'date' => 'Date',
            'release_date' => 'ReleaseDate',
            'delete_date' => 'DeleteDate',
            'time' => 'Time',
            'walker_id' => 'Walker ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHistories()
    {
        return $this->hasMany(History::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaskResources()
    {
        return $this->hasMany(TaskResource::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalker()
    {
        return $this->hasOne(Walker::className(), ['id' => 'walker_id']);
    }

    /**
     * Return tasks, which level more or equals then walker level
     * @param Walker $walker
     * @param array|null $list
     * @return null|ActiveRecord[]
     */
    public static function getTasks($walker, $list = null)
    {
        $query = self::getTasksQueryForWalker($walker);

        if (isset($list) && !empty($list)) {
            $query->andWhere(['in', 'id', $list]);
        }

        $result = $query->all();

        return empty($result) ? null : $result;
    }

    private static function getTasksQueryForWalker($walker)
    {
        $columns = [
            'id',
            'title',
            'info',
            'link',
            'address',
            'latitude',
            'longitude',
            'cost',
            'time'
        ];

        $condition = [
            'AND',
            ['<=', 'level', $walker->level],
            [
                'OR',
                ['status' => Status::AVAILABLE],
                [
                    'AND',
                    ['status' => Status::REJECTED],
                    ['walker_id' => $walker->id],
                ],
            ],
            ['<', 'release_date', time()],
        ];

        return self::find()->select($columns)->where($condition);
    }

    /**
     * Check available status of a task
     * @param int $level
     * @return bool
     */
    public function isCanBeTakenAsAvailable($level)
    {
        if (!$this->isAvailable())  {
            return false;
        }

        if (!$this->isGranted($level)) {
            return false;
        }

        return $this->release_date < time();
    }

    /**
     * Check available status of a task
     * @param int $walkerId
     * @return bool
     */
    public function isCanBeTakenAsRejected($walkerId)
    {
        if (!$this->isRejected())  {
            return false;
        }

        return $this->walker_id == $walkerId;
    }

    /**
     * Check deleted status of a task
     * @return bool
     */
    public function isDeleted()
    {
        return ($this->status == Status::DELETED);
    }

    /**
     * Checks the availability of the task for the walker by level
     * @param int $level
     * @return bool
     */
    public function isGranted($level)
    {
        return $this->level <= $level;
    }

    /**
     * Changes status of a task
     * @param int $status
     */
    public function changeStatus($status)
    {
        if ($status == $this->status) {
            return;
        }
        $this->status = $status;
        $this->save(false);
    }

    /**
     * Returns task as current with resources and time to the end of a task
     * @param int|null $walkerId
     * @param string $startTime
     * @return array|null|\yii\db\ActiveQuery[]
     */
    public function getWithResources($walkerId = null, $startTime = null)
    {
        $task = $this->getInfoForWalker();
        if ($startTime) {
            $task['time'] = Definitions::getDateDiffFromCurrent($task['time'], $startTime);
        }

        $task['resources'] = TaskResource::getResourcesForTask($this->id, $walkerId);
        return $task;
    }

    /**
     * Checks release_data field and sets value before executing parent::reload
     * @param array $data
     * @param string|null $formName
     * @return bool
     */
    public function load($data, $formName = null)
    {
        if(isset($data['Task'])) {
            $task = $data['Task'];
            if(isset($task['release_date'])) {
                if($date = strtotime($task['release_date'])) {
                    $data['Task']['release_date'] = $date;
                } else {
                    $data['Task']['release_date'] = empty($this->release_date) ? time() : $this->release_date;
                }
            } else {
                $data['Task']['release_date'] = empty($this->release_date) ? time() : $this->release_date;
            }
        } else {
            if(!isset($data['release_date'])) {
                $data['release_date'] = time();
            } else {
                if($data['release_date'] <= 0) $data['release_date'] = time();
            }
        }
        return parent::load($data, $formName);
    }

    /**
     * Return available tasks
     * @return array|ActiveRecord[]
     */
    public static function getActiveTasks()
    {
        return $tasks = self::find()->where([
            'AND',
            ['status' => [Status::AVAILABLE, Status::PERFORMED]],
            ['<', 'release_date', time()]
        ])->all();
    }

    /**
     * Return a task info for walker
     * @return array
     */
    public function getInfoForWalker()
    {
        return $this->getAttributes([
            'id',
            'title',
            'info',
            'link',
            'address',
            'latitude',
            'longitude',
            'cost',
            'time'
        ]);
    }

    /**
     * Marks the task as deleted
     */
    public function markAsDeleted()
    {
        $this->updateAttributes([
            'status' => Status::DELETED,
            'delete_date' => time()
        ]);
    }

    /**
     * Check available status of a task
     * @return bool
     */
    public function isAvailable()
    {
        return $this->status == Status::AVAILABLE;
    }

    /**
     * Check rejected status of a task
     * @return bool
     */
    public function isRejected()
    {
        return $this->status == Status::REJECTED;
    }
}
