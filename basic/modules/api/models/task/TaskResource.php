<?php

namespace app\modules\api\models\task;

use app\modules\api\models\constants\resource\Resource;
use app\modules\api\models\walker\Walker;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task_resource".
 *
 * @property integer $id
 * @property integer $task_id
 * @property integer $walker_id
 * @property string $content
 * @property string $comment
 * @property string $field_id
 * @property string $field_name
 * @property string $images
 * @property Task $task
 * @property Walker $walker
 */
class TaskResource extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task_resource';
    }

    /**
     * Remove old resources and upload new
     * @param array $resources
     * @param int $taskId
     * @param int $walkerId
     */
    public static function removeAndAddResources($resources, $taskId, $walkerId = null)
    {
        $condition = [
            'task_id' => $taskId,
            'walker_id' => $walkerId
        ];
		self::deleteAll($condition);

		if ($resources) {
            foreach ($resources as $res) {
                $arr = [];
                $arr['id'] = null;
                $arr['task_id'] = $taskId;
                $arr['walker_id'] = $walkerId;
                $arr['field_id'] = $res['field_id'];
                $arr['field_name'] = $res['field_name'];
                $arr['comment'] = isset($res['comment']) && !empty($res['comment']) ? $res['comment'] : null;
                $arr['images'] = isset($res['images']) && !empty($res['images'][0]) ? $res['images'] : null;
                $arr['images'] = is_array($arr['images']) ? json_encode($res['images'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) : $arr['images'];
                $arr['content'] = isset($res['content']) ? $res['content'] : null;
                $arr['content'] = is_array($arr['content']) ? json_encode($res['content'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES) : $arr['content'];
                if ($arr['content'] == "[\"\"]") {
                    $arr['content'] = null;
                }
                $model = new self();
                $model->load($arr, '');
                $model->save(false);
            }
        }
    }

    /**
     * Create copies of default resources for walker
     * @param $taskId
     * @param $walkerId
     */
    public static function duplicateResources($taskId, $walkerId)
    {
        $defaultResources = self::find()->where(['task_id' => $taskId, 'walker_id' => null])->all();
        $walkerResources = self::find()->where(['task_id' => $taskId, 'walker_id' => $walkerId])->all();
        if ($walkerResources == null && $defaultResources != null) {
            foreach ($defaultResources as $res) {
                $newRes = new TaskResource();
                $newRes->setAttributes($res->attributes);
                $newRes->walker_id = $walkerId;
                $newRes->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'walker_id','field_id', 'field_name'], 'required'],
            [['task_id', 'walker_id'], 'integer'],
            [['content', 'comment', 'images'], 'string'],
            [['field_id'], 'string', 'max' => 50],
            [['field_name'], 'string', 'max' => 255],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['walker_id'], 'exist', 'skipOnError' => true, 'targetClass' => Walker::className(), 'targetAttribute' => ['walker_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'walker_id' => 'Walker ID',
            'content' => 'Content',
            'comment' => 'Comment',
            'field_id' => 'Field ID',
            'field_name' => 'Field Name',
            'images' => 'Images'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWalker()
    {
        return $this->hasOne(Walker::className(), ['id' => 'walker_id']);
    }

    /**
     * @param $taskId
     * @param null $walkerId
     * @return array|ActiveRecord[]
     */
    public static function getResourcesForTask($taskId, $walkerId = null)
    {
        $condition = [
            'task_id' => $taskId,
            'walker_id' => $walkerId
        ];
        $resources =  self::find()->where($condition)->all();

        $arr = ArrayHelper::getColumn($resources, function($elem) {
            $row['content'] = !empty($elem['content']) ? json_decode($elem['content']) : null; //убрать true когда нужно будет возвращать как объект
            $row['images'] = !empty($elem['images']) ? json_decode($elem['images']) : null;
            $row['comment'] = $elem['comment'];
            $row['field_id'] = $elem['field_id'];
            $row['field_name'] = $elem['field_name'];
            return $row;
        });

        return empty($arr) ? null : $arr;
    }

    /**
     * Return array of modified field content
     * @param \stdClass|array|ActiveRecord|\yii\base\Model $res
     * @return array
     */
    public static function parseContent($res)
    {
        $field = 'content';
        $content = $res[$field];

        if ($res['field_id'] == Resource::SCHEDULE) {
            $json = [];
            if(!is_array($content)) {
                $content = json_decode($content);
            }

            if (empty($content)) {
                return null;
            }

            foreach ($content as $day=>$key) {
                if (!empty($key[0])) {
                    $json[$day] = $key;
                }
            }
            $content = $json;
        }

        return $content;
    }

    /**
     * Parse content for view
     * @param $res
     * @return array|null
     */
    public static function getContentForView($res) {
        $field = 'content';
        if (!isset($res[$field]) && empty($res[$field])) {
            return null;
        }

        $content = $res[$field];

        if ($res['field_id'] == Resource::SCHEDULE) {
            $json = [];
            $days = ['d1' => 'ПН', 'd2' => 'ВТ', 'd3' => 'СР', 'd4' => 'ЧТ', 'd5' => 'ПТ', 'd6' => 'СБ', 'd7' => 'ВС'];
            if ($content == [""]) return $content;
            foreach ($content as $day=>$key) {
                $_day = $days[$day];
                $str = '';
                $str .= $_day . ': ' . $key[0] . '-' . $key[1];
                if (sizeof($key[2]) > 1) {
                    $str .= '. Перерыв: ' . $key[2] . '-' . $key[3];
                }
                $json[] = $str;
            }
            $content = empty($json) ? null : $json;
        }

        return $content;
    }
}