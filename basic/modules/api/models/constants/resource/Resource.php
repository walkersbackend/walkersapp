<?php

namespace app\modules\api\models\constants\resource;

/**
 * Class of resources constants
 * @package app\modules\api\models\constants\resource
 */
class Resource
{
    /**
     * @const string
     */
    const COMPANY_NAME = 'company_name';

    /**
     * @const string
     */
    const ACTIVITY_TYPE = 'activity_type';

    /**
     * @const string
     */
    const COMPANY_EMAIL = 'company_email';

    /**
     * @const string
     */
    const COMPANY_SITE = 'company_site';

    /**
     * @const string
     */
    const COMPANY_INFO = 'company_info';

    /**
     * @const string
     */
    const COMPANY_PHONE = 'company_phone';

    /**
     * @const string
     */
    const CONTACT_PHONE = 'contact_phone';

    /**
     * @const string
     */
    const CONTACT_NAME = 'contact_name';

    /**
     * @const string
     */
    const CONTACT_SCHEDULE = 'contact_schedule';

    /**
     * @const string
     */
    const ADDRESS = 'address';

    /**
     * @const string
     */
    const SCHEDULE = 'schedule';

    /**
     * @const string
     */
    const PAYMENTS = 'payments';

    /**
     * @const string
     */
    const EXTRA = 'extra';

    public static function getFieldIds() {
        return [
            self::ACTIVITY_TYPE,
            self::CONTACT_SCHEDULE,
            self::CONTACT_NAME,
            self::CONTACT_PHONE,
            self::COMPANY_SITE,
            self::COMPANY_PHONE,
            self::COMPANY_INFO,
            self::COMPANY_EMAIL,
            self::COMPANY_NAME,
            self::ADDRESS,
            self::PAYMENTS,
            self::SCHEDULE,
            self::EXTRA
        ];
    }


}