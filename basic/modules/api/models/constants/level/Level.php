<?php

namespace app\modules\api\models\constants\level;

/**
 * Class of level constants
 * @package app\modules\api\models\constants\level
 */
class Level
{
    /**
     * Value of basic level
     * @const int
     */
    const BASIC = 1;

    /**
     * Value of advanced level
     * @const int
     */
    const ADVANCED = 2;

    /**
     * Value of expert level
     * @const int
     */
    const EXPERT = 3;

    /**
     * Return levels list
     * @return array
     */
    public static function getLevels()
    {
        return [
            self::BASIC,
            self::ADVANCED,
            self::EXPERT
        ];
    }

    public static function getLabels()
    {
        return [
            'Базовый',
            'Любитель',
            'Доверенный'
        ];
    }

    public static function getLabelByLevel($value = null)
    {
        $arr = array_combine(self::getLevels(), self::getLabels());
        if ($value) {
            return $arr[$value];
        }

        return $arr;
    }
}