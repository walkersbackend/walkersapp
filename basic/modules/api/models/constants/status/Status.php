<?php

namespace app\modules\api\models\constants\status;

/**
 * Class of status constants
 * @package app\modules\api\models\constants\status
 */
class Status
{
    /**
     * value of available status
     * @const int
     */
    const AVAILABLE = 1;

    /**
     * Value of performed status
     * @const int
     */
    const PERFORMED = 2;

    /**
     * Value of completed status
     * @const int
     */
    const COMPLETED = 3;

    /**
     * Value of deleted status
     * @const int
     */
    const DELETED = 4;

    /**
     * Value of moderated result
     * @const int
     */
    const MODERATED = 5;

    /**
     * Value of rejected result
     * @const int
     */
    const REJECTED = 6;

    /**
     * Return statuses list
     * @return array
     */
    public static function getStatuses() {
        return [
            self::AVAILABLE,
            self::PERFORMED,
            self::COMPLETED,
            self::DELETED,
            self::MODERATED,
            self::REJECTED
        ];
    }

    /**
     * Return status labels
     * @return array
     */
    public static function getLabels() {
        return [
            'Доступна',
            'Выполняется',
            'Завершена',
            'Удалена',
            'На модерации',
            'Возвращена'
        ];
    }

    /**
     * Return status label by status
     * @param int $status
     * @return mixed
     */
    public static function getLabelByStatus($status) {
        return self::getList()[$status];
    }

    /**
     * Return assoc array with status and labels
     * @return array
     */
    public static function getList() {
        return array_combine(self::getStatuses(), self::getLabels());
    }
}