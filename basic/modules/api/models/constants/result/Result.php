<?php

namespace app\modules\api\models\constants\result;

/**
 * Class of result constants
 * @package app\modules\api\models\constants\result
 */
class Result
{
    /**
     * Value of cancelled result
     * @const int
     */
    const CANCELED = 1;

    /**
     * Value of completed result
     * @const int
     */
    const COMPLETED = 2;

    /**
     * Value of moderated result
     * @const int
     */
    const MODERATED = 3;

    /**
     * Value of rejected result
     * @const int
     */
    const REJECTED = 4;

    /**
     * Return results list
     * @return array
     */
    public static function getResults()
    {
        return [
            self::CANCELED,
            self::COMPLETED,
            self::MODERATED,
            self::REJECTED
        ];
    }
}