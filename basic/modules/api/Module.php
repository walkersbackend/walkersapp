<?php

namespace app\modules\api;

use yii\web\Response;
use Yii;

/**
 * api module definition class
 */
class Module extends \yii\base\Module {
    /**
     * @inheritdoc
     */

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->modules = [
            'client' => [
                'class' => 'app\modules\api\modules\client\Module',
            ],
            'project' => [
                'class' => 'app\modules\api\modules\project\Module',
            ],
        ];

        Yii::$app->response->format = Response::FORMAT_JSON;
    }
}
