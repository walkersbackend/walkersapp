<?php

namespace app\modules\api\reload;

use app\modules\api\models\history\History;

/**
 * Class TaskReload
 * @package app\modules\api\reload
 */
class TaskReload
{
    /**
     * Runs tasks reload
     * Reloads tasks, which execution time is up, and make available tasks, which release date less then current date
     */
    public function run()
    {
        $time = time();
        History::updateCurrentTasks($time);
        History::updateRejectedTasks($time);
    }
}