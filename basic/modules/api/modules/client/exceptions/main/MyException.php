<?php

namespace app\modules\api\modules\client\exceptions\main;

/**
 * Class-parent for custom exceptions
 * @package app\modules\api\modules\client\exceptions\main
 */
class MyException extends \Exception
{
    /**
     * Error body
     * @var array
     */
    protected $errorBody = [];

    /**
     * Extra
     * @var array
     */
    protected $extra = [];

    /**
     * Return field $errorBody
     * @return array
     */
    public function getErrorBody()
    {
        \Yii::info([
            'short' => $this->errorBody['message'],
            'error' => $this->errorBody,
            'extra' => $this->extra
        ]);
        return $this->errorBody;
    }
}