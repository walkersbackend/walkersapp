<?php

namespace app\modules\api\modules\client\exceptions\walker;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class NotFoundLastTaskException
 * @package app\modules\api\modules\client\exceptions\walker
 */
class NotFoundLastTaskException extends MyException
{
    /**
     * NotFoundLastTaskException constructor.
     */
    public function __construct()
    {
        $this->errorBody = Errors::getBody(Errors::NOT_FOUND_LAST_TASK);
    }
}