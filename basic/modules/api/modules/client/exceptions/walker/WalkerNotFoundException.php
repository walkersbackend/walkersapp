<?php

namespace app\modules\api\modules\client\exceptions\walker;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class WalkerNotFoundException
 * @package app\modules\api\modules\client\exceptions
 */
class WalkerNotFoundException extends MyException
{
    /**
     * @var string
     */
    private $email = 'email';

    /**
     * @var string
     */
    private $token = 'token';

    /**
     * @var string
     */
    private $id    = 'token';

    /**
     * WalkerNotFoundException constructor.
     * @param string $field
     */
    public function __construct($field)
    {
        $error = null;
        switch ($field) {
            case $this->email:
                $error = Errors::WALKER_NOT_FOUND_BY_EMAIL;
                break;
            case $this->token:
                $error = Errors::WALKER_NOT_FOUND_BY_TOKEN;
                break;
            case $this->id:
                $error = Errors::WALKER_NOT_FOUND_BY_ID;
                break;
            default:
        }

        if ($error) {
            $this->errorBody = Errors::getBody($error);
        }
    }
}