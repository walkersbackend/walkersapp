<?php

namespace app\modules\api\modules\client\exceptions\walker;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class NotFindCurrentTaskException
 * @package app\modules\api\modules\client\services\TaskService
 */
class NotFoundCurrentTaskException extends MyException
{
    /**
     * NotFindCurrentTaskException constructor.
     */
    public function __construct()
    {
        $this->errorBody = Errors::getBody(Errors::NOT_FOUND_CURRENT_TASK);
    }
}