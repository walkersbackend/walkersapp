<?php

namespace app\modules\api\modules\client\exceptions\walker;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class WalkerAlreadyPerformingException
 * @package app\modules\api\modules\client\exceptions
 */
class WalkerAlreadyPerformException extends MyException
{
    /**
     * WalkerAlreadyPerformException constructor.
     */
    public function __construct()
    {
        $this->errorBody = Errors::getBody(Errors::WALKER_ALREADY_PERFORMING);
    }
}