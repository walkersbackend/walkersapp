<?php

namespace app\modules\api\modules\client\exceptions\task;

use app\modules\api\models\errors\Errors;
use app\modules\api\models\task\Task;
use app\modules\api\modules\client\exceptions\main\MyException;

abstract class TaskMainException extends MyException
{
    /**
     * @var
     */
    protected $errorConst;

    /**
     * TaskMainException constructor.
     * @param Task|null $task
     */
    final public function __construct(Task $task = null)
    {
        if (isset($task)) {
            $this->extra = $task->getAttributes([
                'id',
                'status',
                'walker_id',
            ]);
        }

        if (isset($this->errorConst)) {
            $this->errorBody = Errors::getBody($this->errorConst);
        }
    }
}