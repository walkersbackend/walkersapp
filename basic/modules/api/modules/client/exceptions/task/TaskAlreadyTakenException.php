<?php

namespace app\modules\api\modules\client\exceptions\task;

use app\modules\api\models\errors\Errors;

/**
 * Class TaskAlreadyTakenException
 * @package app\modules\api\modules\client\exceptions
 */
class TaskAlreadyTakenException extends TaskMainException
{
    /**
     * @var int
     */
    protected $errorConst = Errors::TASK_NOT_AVAILABLE;
}