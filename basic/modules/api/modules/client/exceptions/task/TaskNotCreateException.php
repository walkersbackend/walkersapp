<?php

namespace app\modules\api\modules\client\exceptions\task;

use app\modules\api\models\errors\Errors;

/**
 * Class TaskNotCreateException
 * @package app\modules\api\modules\client\exceptions\task
 */
class TaskNotCreateException extends TaskMainException
{
    /**
     * @var int
     */
    protected $errorConst = Errors::TASK_NOT_CREATE;
}