<?php

namespace app\modules\api\modules\client\exceptions\task;

use app\modules\api\models\errors\Errors;

/**
 * Class TaskNotGrantedException
 * @package app\modules\api\modules\client\exceptions
 */
class TaskNotGrantedException extends TaskMainException
{
    /**
     * @var int
     */
    protected $errorConst = Errors::TASK_NOT_GRANTED;
}