<?php

namespace app\modules\api\modules\client\exceptions\task;

use app\modules\api\models\errors\Errors;

/**
 * Class TaskNotFoundException
 * @package app\modules\api\modules\client\exceptions
 */
class TaskNotFoundException extends TaskMainException
{
    /**
     * @var int
     */
    protected $errorConst = Errors::TASK_NOT_FOUND;
}