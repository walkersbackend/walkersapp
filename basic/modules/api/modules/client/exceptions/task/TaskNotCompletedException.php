<?php

namespace app\modules\api\modules\client\exceptions\task;

use app\modules\api\models\errors\Errors;

/**
 * Class TaskNotCompletedException
 * @package app\modules\api\modules\client\exceptions\task
 */
class TaskNotCompletedException extends TaskMainException
{
    /**
     * @var int
     */
    protected $errorConst = Errors::TASK_NOT_COMPLETED;
}