<?php

namespace app\modules\api\modules\client\exceptions\resource;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;
use Throwable;

/**
 * Class FileNotLoadException
 * @package app\modules\api\modules\client\exceptions\resource
 */
class FileNotLoadException extends MyException
{
    /**
     * FileNotLoadException constructor.
     */
    public function __construct()
    {
        $this->errorBody = Errors::getBody(Errors::FILE_NOT_LOAD);
    }
}