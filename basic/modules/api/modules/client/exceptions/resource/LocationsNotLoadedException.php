<?php

namespace app\modules\api\modules\client\exceptions\resource;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class LocationsNotLoadedException
 * @package app\modules\api\modules\client\exceptions\resource
 */
class LocationsNotLoadedException extends MyException
{
    /**
     * LocationsNotLoadedException constructor.
     */
    public function __construct()
    {
        $this->errorBody = Errors::getBody(Errors::LOCATION_NOT_LOADED);
    }
}