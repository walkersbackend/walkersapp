<?php

namespace app\modules\api\modules\client\exceptions\resource;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class ResourcesNotLoadedException
 * @package app\modules\api\modules\client\exceptions
 */
class ResourcesNotLoadedException extends MyException
{
    /**
     * ResourcesNotLoadedException constructor.
     */
    public function __construct()
    {
        $this->errorBody = Errors::getBody(Errors::RESOURCE_NOT_LOADED);
    }
}