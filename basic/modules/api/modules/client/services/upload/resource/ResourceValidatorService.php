<?php

namespace app\modules\api\modules\client\services\upload\resource;

use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\resource\ResourcesNotLoadedException;
use app\modules\api\modules\client\forms\ResourcesForm;
use Yii;
use yii\helpers\BaseFileHelper;

/**
 * Class for validating resource
 * @package app\modules\api\modules\client\services\resource
 */
class ResourceValidatorService
{
    /**
     * @var bool
     */
    private $needSkipInvalidResources = false;

    /**
     * Run validate
     * @param $data array
     * @return array
     * @throws ResourcesNotLoadedException
     */
    public function validate($data)
    {
        $data = isset($data['resources']) ? $data['resources'] : null;
        try {
            if (!$data) {
                throw new ResourcesNotLoadedException;
            }

            if (!$this->validateFields($data)) {
                throw new ResourcesNotLoadedException;
            }

            if (isset($data['images']) && !$this->validateFiles($data)) {
                throw new ResourcesNotLoadedException;
            }
        } catch (MyException $exception) {
            if ($this->needSkipInvalidResources) {
                return null;
            }
            throw new ResourcesNotLoadedException;
        }

        return $data;
    }

    /**
     * Return baseName of files
     * @param string $path
     * @param array $array
     * @return array
     */
    private function prepareFiles($path, $array)
    {
        $arr = [];
        foreach ($array as $a) {
            $arr[] = str_replace($path, '', $a);
        }
        return $arr;
    }

    /**
     * Validate resource fields
     * @param array &$data
     * @return bool
     */
    private function validateFields(&$data)
    {
        $form = new ResourcesForm;
        $dataCopy = [];
        foreach ($data as $res) {
            if (!$form->load($res, '') || !$form->validate()) {
                if (!$this->needSkipInvalidResources) {
                    return false;
                }
                $form->clear();
                continue;
            }

            $dataCopy[] = $form->getAsArray();
            $form->clear();
        }

        if ($dataCopy != $data) {
            $data = $dataCopy;
        }

        return true;
    }

    /**
     * Validate file names
     * @param array $data
     * @return bool
     */
    private function validateFiles(&$data)
    {
        $globalPath = Yii::getAlias('@uploadsGlobal') . '/';
        $localPath = Yii::getAlias('@uploads') . '/';
        $localFilesArray = $this->prepareFiles($localPath, BaseFileHelper::findFiles($localPath));
        $valid_data = [];
        foreach ($data as $res) {
            $files = $res['images'];
            $globalFilesArray = $this->prepareFiles($globalPath, $files);
            foreach ($globalFilesArray as $file) {
                if (empty($file) || !in_array($file, $localFilesArray)) {
                    continue;
                }
                $valid_data[] = $res;
            }
        }
        if ($data != $valid_data) {
            $data = $valid_data;
            return false;
        }

        return true;
    }

    /**
     * @param $bool
     * @return $this
     */
    public function setSkipInvalidResources($bool) {
        $this->needSkipInvalidResources = $bool;
        return $this;
    }
}