<?php

namespace app\modules\api\modules\client\services\upload\file;

use app\modules\api\modules\client\exceptions\resource\FileNotLoadException;
use app\modules\api\modules\client\forms\FileForm;
use Yii;

/**
 * Class for upload file
 * @package app\modules\api\modules\client\services\upload\file
 */
class FileUploader
{
    /**
     * Upload file and return link
     * @param array $data
     * @return string
     * @throws FileNotLoadException
     */
    public function upload($data)
    {
        $data = isset($data['file']) ? $data['file'] : null;
        if (!$data) {
            throw new FileNotLoadException;
        }

        $form = new FileForm;
        if (!$form->load($data, '') || !$form->validate()) {
            throw new FileNotLoadException;
        }

        $fileNames = $this->generateNames($form->getExtension());

        if (!move_uploaded_file($form->tmp_name, $fileNames['localName'])) {
            throw new FileNotLoadException;
        }

        return $fileNames['globalName'];
    }

    /**
     * Generates file names
     * @param  string $extension
     * @return array
     */
    private function generateNames($extension)
    {
        $dir = Yii::getAlias('@uploads');
        $fileName = md5(microtime() . rand(0, 9999));
        $localPath = $dir . '/'. $fileName . $extension;
        while (file_exists($localPath)) {
            $fileName = md5(microtime() . rand(0, 9999));
            $localPath = $dir . '/' . $fileName . $extension;
        }

        return [
            'localName' => $localPath,
            'globalName' => Yii::getAlias('@uploadsGlobal') . '/' . $fileName . $extension
        ];
    }


}