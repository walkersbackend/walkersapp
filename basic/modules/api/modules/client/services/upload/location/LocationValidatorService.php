<?php

namespace app\modules\api\modules\client\services\upload\location;

use app\modules\api\modules\client\exceptions\resource\LocationsNotLoadedException;
use app\modules\api\modules\client\forms\LocationForm;

/**
 * Class for validate locations
 * @package app\modules\api\modules\client\services\upload\location
 */
class LocationValidatorService
{
    /**
     * Validate locations
     * @param  array $data
     * @return array
     * @throws LocationsNotLoadedException
     */
    public function validate($data)
    {
        $validateData = [];
        $data = isset($data['locations']) ? $data['locations'] : null;
        if (!$data) {
            throw new LocationsNotLoadedException;
        }

        $form = new LocationForm();
        foreach ($data as $location) {
            if ($form->load($location, '') && $form->validate()) {
                $validateData[] = $location;
            }

            $form->clear();
        }

        if (empty($validateData)) {
            throw new LocationsNotLoadedException;
        }

        return $validateData;
    }
}