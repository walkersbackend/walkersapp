<?php

namespace app\modules\api\modules\client\services\auth;

use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\services\auth\adapter\IWalkerAuthAdapter;
use app\modules\api\modules\client\services\auth\result\WalkerAuthResult;

/**
 * Interface IAuthService
 * @package app\modules\api\modules\client\services\auth
 */
abstract class AbstractWalkerAuthService
{
    /**
     * @param IWalkerAuthAdapter $adapter
     * @return Walker
     * @throws MyException
     */
    final public function auth(IWalkerAuthAdapter $adapter)
    {
        $result = $adapter->auth();

        if ($result->isSuccess()) {
            $this->afterAuth($result);
            return $result->getIdentity();
        }

        throw $result->getException();
    }

    /**
     * Function executed only when $result->isSuccess() is true
     * @param WalkerAuthResult $result
     */
    abstract function afterAuth(WalkerAuthResult $result);
}