<?php

namespace app\modules\api\modules\client\services\auth\adapter\token;

use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\walker\WalkerNotFoundException;
use app\modules\api\modules\client\forms\TokenForm;
use app\modules\api\modules\client\services\auth\adapter\AbstractWalkerAuthAdapter;
use app\modules\api\modules\client\services\auth\result\WalkerAuthResult;

/**
 * Class WalkerAuthByTokenAdapter
 * @package app\modules\api\modules\client\services\auth\adapter\token
 */
class WalkerAuthByTokenAdapter extends AbstractWalkerAuthAdapter
{
    /**
     * @var string
     */
    private $field = 'token';

    /**
     * @return WalkerAuthResult
     */
    public function auth()
    {
        $form = new TokenForm();
        if (!$form->load($this->data, '') || !$form->validate()) {
            return new WalkerAuthResult(
                WalkerAuthResult::NOT_FOUND_BY_TOKEN,
                null,
                new WalkerNotFoundException($this->field)
            );
        }

        $walker = Walker::findOne([$this->field => $form->token]);
        if (!$walker) {
            return new WalkerAuthResult(
                WalkerAuthResult::NOT_FOUND_BY_TOKEN,
                null,
                new WalkerNotFoundException($this->field)
            );
        }

        return new WalkerAuthResult(
            WalkerAuthResult::SUCCESS,
            $walker
        );
    }
}