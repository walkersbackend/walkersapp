<?php

namespace app\modules\api\modules\client\services\auth\adapter\email;

use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\walker\WalkerNotFoundException;
use app\modules\api\modules\client\forms\EmailForm;
use app\modules\api\modules\client\services\auth\adapter\AbstractWalkerAuthAdapter;
use app\modules\api\modules\client\services\auth\result\WalkerAuthResult;

/**
 * Class WalkerAuthByEmailAdapter
 * @package app\modules\api\modules\client\services\auth\adapter\email
 */
class WalkerAuthByEmailAdapter extends AbstractWalkerAuthAdapter
{
    private $field = 'email';

    /**
     * @return WalkerAuthResult
     */
    public function auth()
    {
        $form = new EmailForm();
        if (!$form->load($this->data, '') || !$form->validate()) {
            return new WalkerAuthResult(
                WalkerAuthResult::NOT_FOUND_BY_EMAIL,
                null,
                new WalkerNotFoundException($this->field)
            );
        }

        $walker = Walker::findOne([$this->field => $form->email]);
        if (!$walker) {
            return new WalkerAuthResult(
                WalkerAuthResult::NOT_FOUND_BY_EMAIL,
                null,
                new WalkerNotFoundException($this->field)
            );
        }

        return new WalkerAuthResult(
            WalkerAuthResult::SUCCESS,
            $walker
        );
    }
}