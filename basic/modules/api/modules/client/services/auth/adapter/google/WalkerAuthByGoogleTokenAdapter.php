<?php

namespace app\modules\api\modules\client\services\auth\adapter\google;

use app\modules\api\modules\client\exceptions\walker\WalkerNotFoundException;
use app\modules\api\modules\client\services\auth\adapter\AbstractWalkerAuthAdapter;
use app\modules\api\modules\client\services\auth\adapter\email\WalkerAuthByEmailAdapter;
use app\modules\api\modules\client\services\auth\result\WalkerAuthResult;
use Google_Client;

class WalkerAuthByGoogleTokenAdapter extends AbstractWalkerAuthAdapter
{
    /**
     * @return WalkerAuthResult
     */
    public function auth()
    {
        $client = new Google_Client();
        $email = null;
        if (isset($this->data['id_token'])) {
            try {
                $result = $client->verifyIdToken($this->data['id_token']);
                if (isset($result['email'])) {
                    $email = ['email' => $result['email']];
                }
            } catch (\Exception $e) {

            }
        }

        if (!$email) {
            return new WalkerAuthResult(
                WalkerAuthResult::NOT_FOUND_BY_GOOGLE_TOKEN,
                null,
                new WalkerNotFoundException('token')
            );
        }

        return (new WalkerAuthByEmailAdapter($email))->auth();
    }
}