<?php

namespace app\modules\api\modules\client\services\auth\adapter;

/**
 * Class AbstractWalkerAuthAdapter
 * @package app\modules\api\modules\client\services\auth\adapter
 */
abstract class AbstractWalkerAuthAdapter implements IWalkerAuthAdapter
{
    /**
     * @var mixed|null
     */
    protected $data;

    /**
     * AbstractWalkerAuthAdapter constructor.
     * @param null|mixed $data
     */
    final public function __construct($data = null)
    {
        $this->data = $data;
    }
}