<?php

namespace app\modules\api\modules\client\services\auth\adapter;

use app\modules\api\modules\client\services\auth\result\WalkerAuthResult;

/**
 * Interface IWalkerAuthAdapter
 * @package app\modules\api\modules\client\services\auth\adapter
 */
interface IWalkerAuthAdapter
{
    /**
     * @return WalkerAuthResult
     */
    public function auth();
}