<?php

namespace app\modules\api\modules\client\services\auth\result;

use app\modules\api\models\service\result\AbstractResult;
use app\modules\api\models\walker\Walker;

class WalkerAuthResult extends AbstractResult
{
    const NOT_FOUND_BY_EMAIL = 2;

    const NOT_FOUND_BY_TOKEN = 3;

    const NOT_FOUND_BY_GOOGLE_TOKEN = 4;

    /**
     * @var Walker
     */
    protected $identity;

    /**
     * @return Walker
     */
    public function getIdentity()
    {
        return $this->identity;
    }
}