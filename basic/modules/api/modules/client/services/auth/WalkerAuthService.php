<?php

namespace app\modules\api\modules\client\services\auth;

use app\modules\api\modules\client\services\auth\result\WalkerAuthResult;

/**
 * Class for authorize walker
 * @package app\modules\api\modules\client\services\auth
 */
class WalkerAuthService extends AbstractWalkerAuthService
{
    /***
     * Method updates field `user_agent` in model Walker
     *
     * @inheritdoc
     */
    function afterAuth(WalkerAuthResult $result)
    {
        $result->getIdentity()->updateAttributes([
            'user_agent' => \Yii::$app->request->userAgent,
            'last_activity' => time()
        ]);
    }
}