<?php

namespace app\modules\api\modules\client\services\task\find;

/**
 * Interface ITaskFinderService
 * @package app\modules\api\modules\client\services\task\find
 */
interface ITaskFinderService
{
    /**
     * @param array $data
     * @return \app\modules\api\models\task\Task
     */
    public function find($data);
}