<?php

namespace app\modules\api\modules\client\services\task\find;

use app\modules\api\models\task\Task;
use app\modules\api\modules\client\exceptions\task\TaskNotFoundException;
use app\modules\api\modules\client\forms\TaskForm;

/**
 * Class for find Task by id
 * @package app\modules\api\modules\client\services\task\find
 */
class TaskFinderService implements ITaskFinderService
{
    /**
     * Find task by id
     * @param array $data
     * @return Task
     * @throws TaskNotFoundException
     */
    public function find($data)
    {
        $form = new TaskForm;
        if (!$form->load($data, '') || !$form->validate()) {
            throw new TaskNotFoundException;
        }

        $task = Task::findOne($form->task_id);
        if (!$task) {
            throw new TaskNotFoundException;
        }

        return $task;
    }
}