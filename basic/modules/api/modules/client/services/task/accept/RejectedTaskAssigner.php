<?php

namespace app\modules\api\modules\client\services\task\accept;

use app\modules\api\models\history\History;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\task\TaskAlreadyTakenException;
use app\modules\api\modules\client\services\task\assign\AbstractTaskAssigner;

/**
 * Class TaskAcceptService
 * @package app\modules\api\modules\client\services\task\accept
 */
class RejectedTaskAssigner extends AbstractTaskAssigner
{
    /**
     * @throws MyException $e
     */
    protected function check()
    {
        $history = History::getRejectedHistory($this->walker->id, $this->task->id);

        if (!$history) {
            throw new TaskAlreadyTakenException($this->task);
        }

        if (!$this->task->isCanBeTakenAsRejected($this->walker->id)) {
            throw new TaskAlreadyTakenException($this->task);
        }

        $this->history = $history;
    }
}