<?php

namespace app\modules\api\modules\client\services\task\back;

use app\modules\api\models\task\Task;
use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\walker\NotFoundLastTaskException;

/**
 * Interface ITaskReturnerService
 * @package app\modules\api\modules\client\services\task\back
 */
interface ITaskReturnerService
{
    /**
     * @param Walker $walker
     * @return Task
     * @throws NotFoundLastTaskException
     */
    public function back($walker);
}