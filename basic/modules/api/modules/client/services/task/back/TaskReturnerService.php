<?php

namespace app\modules\api\modules\client\services\task\back;

use app\modules\api\models\history\History;
use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\task\TaskAlreadyTakenException;
use app\modules\api\modules\client\exceptions\walker\NotFoundLastTaskException;
use app\modules\api\modules\client\services\task\assign\CompletedOrCancelledTaskAssigner;

/**
 * Class TaskReturnerService
 * @package app\modules\api\modules\client\services\task\back
 */
class TaskReturnerService implements ITaskReturnerService
{
    /**
     * @param Walker $walker
     * @return array|\yii\db\ActiveQuery[]
     * @throws NotFoundLastTaskException
     * @throws TaskAlreadyTakenException
     */
    public function back($walker)
    {
        $history = History::getLastCancelledOrCompletedTask($walker);
        if (!$history) {
            throw new NotFoundLastTaskException();
        }

        $task = $history->task;

        (new CompletedOrCancelledTaskAssigner($walker, $task, $history))->assign();

        return $task->getWithResources($walker->id, $history->start_time);
    }
}