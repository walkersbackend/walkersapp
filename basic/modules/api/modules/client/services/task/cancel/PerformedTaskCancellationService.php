<?php

namespace app\modules\api\modules\client\services\task\cancel;

use app\modules\api\models\history\History;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\walker\WalkerAlreadyPerformException;

class PerformedTaskCancellationService extends AbstractTaskCancellationService
{
    /**
     * @return boolean
     * @throws MyException
     */
    protected function prepare()
    {
        $history = History::getCurrentHistory($this->walker);

        if (!$history) {
            throw new WalkerAlreadyPerformException();
        }

        $this->history = $history;
        $this->task = $history->task;

        return true;
    }
}