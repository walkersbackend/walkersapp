<?php

namespace app\modules\api\modules\client\services\task\cancel;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\constants\status\Status;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\models\track\Track;
use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\main\MyException;

/**
 * Class for cancellation a task
 * @package app\modules\api\modules\client\services\task\cancel
 */
abstract class AbstractTaskCancellationService
{
    /**
     * @var Walker $walker
     */
    protected $walker;

    /**
     * @var Task $task
     */
    protected $task;

    /**
     * @var History $history
     */
    protected $history;

    /**
     * @var string|null
     */
    protected $reason;

    final public function __construct($walker, $task = null, $reason = null)
    {
        $this->walker = $walker;
        $this->reason = $reason;
        $this->task = $task;
    }

    /**
     * Cancel a task
     */
    final private function _cancel()
    {
        Track::deleteAll(['history_id' => $this->history->id]);

        $this->task->updateAttributes([
            'status' => Status::AVAILABLE,
            'walker_id' => null,
        ]);

        $this->history->updateAttributes([
            'result' => Result::CANCELED,
            'comment' => $this->reason,
            'end_time' => time()
        ]);
    }

    /**
     * @return boolean
     * @throws MyException
     */
    abstract protected function prepare();

    final public function cancel()
    {
        try {
            if ($this->prepare()){
                $this->_cancel();
            }
        } catch (MyException $e) {

        }
    }
}
