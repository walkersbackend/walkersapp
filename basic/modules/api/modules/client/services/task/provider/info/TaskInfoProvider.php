<?php

namespace app\modules\api\modules\client\services\task\provider\info;

use app\modules\api\models\task\Task;
use app\modules\api\models\walker\Walker;

/**
 * Class TaskInfoProvider
 * @package app\modules\api\modules\client\services\provider\info
 */
class TaskInfoProvider
{
    /**
     * @param Walker $walker
     * @param $data
     * @return array
     */
    public function getTasks($walker, $data) {

        if (!isset($data['list']) || empty($data['list'])) {
            return  null;
        }

        $tasks = Task::getTasks($walker, $data['list']);

        $tasksWithResources = null;

        if (isset($tasks)) {
            $tasksWithResources = [];
            /*** @var Task $task */
            foreach ($tasks as $task) {
                $tasksWithResources[] = $task->getWithResources();
            }
        }

        return $tasksWithResources;
    }
}