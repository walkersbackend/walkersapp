<?php

namespace app\modules\api\modules\client\services\task\provider\info;

use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;

/**
 * Class HistoryInfoProvider
 * @package app\modules\api\modules\client\services\task\provider\info
 */
class HistoryInfoProvider
{
    /**
     * Return info about a rejected tasks
     * @param int $walkerId
     * @return array|null
     */
    public function getTasks($walkerId)
    {
        $histories = History::getRejectedTasks($walkerId);

        $arr = [];
        if (isset($histories)) {
            $delta = Task::HOLDING_TIME - time();
            foreach ($histories as $history) {
                $task = $history->task;
                $row = $task->getAttributes(['id', 'title', 'address', 'longitude', 'latitude']);
                $row['comment'] = $history->comment;
                $row['time'] = $history->start_time + $delta;

                $arr[] = $row;
            }
        }

        return $arr;
    }
}