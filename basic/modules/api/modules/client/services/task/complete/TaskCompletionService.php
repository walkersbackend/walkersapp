<?php

namespace app\modules\api\modules\client\services\task\complete;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\constants\status\Status;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\walker\NotFoundCurrentTaskException;

/**
 * Class for completion task
 * @package app\modules\api\modules\client\services\task\complete
 */
class TaskCompletionService implements ITaskCompletionService
{
    /**
     * Complete a task
     * @param Walker $walker
     * @throws NotFoundCurrentTaskException
     */
    public function complete($walker)
    {
        $history = History::getCurrentHistory($walker);
        if (!$history) {
            throw new NotFoundCurrentTaskException;
        }

        $task = Task::findOne(['id' => $history->task_id]);
        $task->changeStatus(Status::MODERATED);

        $history->updateAttributes([
            'result' => Result::MODERATED,
            'end_time' => time()
        ]);
    }
}