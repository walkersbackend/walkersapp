<?php

namespace app\modules\api\modules\client\services\task\complete;

/**
 * Interface ITaskCompletionService
 * @package app\modules\api\modules\client\services\task\complete
 */
interface ITaskCompletionService
{
    /**
     * @param \app\modules\api\models\walker\Walker $walker
     */
    public function complete($walker);
}