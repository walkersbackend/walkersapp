<?php

namespace app\modules\api\modules\client\services\task\reject;

use app\modules\api\models\history\History;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\services\task\cancel\AbstractTaskCancellationService;

class RejectTaskCancellationService extends AbstractTaskCancellationService
{

    /**
     * @throws MyException
     */
    protected function prepare()
    {
        $history = History::getRejectedHistory($this->walker->id, $this->task->id);

        if (!$history) {
             return false;
        }

        $this->history = $history;

        return true;
    }
}