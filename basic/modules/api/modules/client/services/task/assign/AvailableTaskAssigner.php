<?php

namespace app\modules\api\modules\client\services\task\assign;

use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\task\TaskAlreadyTakenException;

/**
 * Class for assign walker on a task
 * @package app\modules\api\modules\client\services\task
 */
class AvailableTaskAssigner extends AbstractTaskAssigner
{
    /**
     * @throws MyException $e
     */
    protected function check()
    {
        if (!$this->task->isCanBeTakenAsAvailable($this->walker->level)) {
            throw new TaskAlreadyTakenException($this->task);
        }
    }
}