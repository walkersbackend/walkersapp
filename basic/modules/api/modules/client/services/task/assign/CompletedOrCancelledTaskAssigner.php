<?php

namespace app\modules\api\modules\client\services\task\assign;

use app\modules\api\models\constants\status\Status;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\task\TaskAlreadyTakenException;

class CompletedOrCancelledTaskAssigner extends AbstractTaskAssigner
{
    /**
     * @throws MyException $e
     */
    protected function check()
    {
        if (!in_array($this->task->status, [Status::MODERATED, Status::AVAILABLE])) {
            throw new TaskAlreadyTakenException($this->task);
        }

        if (isset($this->task->walker_id) && $this->task->walker_id != $this->history->walker_id) {
            throw new TaskAlreadyTakenException($this->task);
        }
    }
}