<?php

namespace app\modules\api\modules\client\services\task\assign;

use app\modules\api\models\constants\status\Status;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\models\task\TaskResource;
use app\modules\api\models\track\Track;
use app\modules\api\models\walker\Walker;
use app\modules\api\modules\client\exceptions\main\MyException;

abstract class AbstractTaskAssigner
{
    /**
     * @var Walker $walker
     */
    protected $walker;

    /**
     * @var Task $task
     */
    protected $task;

    /**
     * @var History $history
     */
    protected $history;

    /**
     * Assigns walker on a task
     */
    final private function _assign()
    {
        if ($this->history) {
            $this->reAssignTask();
        } else {
            $this->assignAsNewTask();
        }

        TaskResource::duplicateResources($this->task->id, $this->walker->id);

        $this->task->updateAttributes([
            'status' => Status::PERFORMED,
            'walker_id' => $this->walker->id
        ]);
    }

    final private function assignAsNewTask()
    {
        new History([
            'walker_id' => $this->walker->id,
            'task_id' => $this->task->id,
            'walker_user_agent' => \Yii::$app->request->userAgent,
            'start_time' => time()
        ]);
    }

    final private function reAssignTask()
    {
        Track::deleteAll(['history_id' => $this->history->id]);

        $this->history->updateAttributes([
            'result' => null,
            'end_time' => null,
            'start_time' => time(),
            'walker_user_agent' => \Yii::$app->request->userAgent
        ]);
    }

    final public function __construct($walker, $task, $history = null)
    {
        $this->walker = $walker;
        $this->task = $task;
        $this->history = $history;
    }

    /**
     * @throws MyException $e
     */
    abstract protected function check();

    /**
     * @throws MyException
     */
    final public function assign()
    {
        try {
            $this->check();
            $this->_assign();
        } catch (MyException $e) {
            throw $e;
        }
    }
}