<?php

namespace app\modules\api\modules\client\controllers;

use app\modules\api\models\response\ResponseBody;
use yii\web\Controller;

/**
 * Class BaseController
 * @package app\modules\api\modules\client\controllers
 */
class BaseController extends Controller
{
    /**
     * @var ResponseBody
     */
    protected $response;

    /**
     * BaseController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param array $config
     */
    public function __construct($id, $module, $config = [])
    {
        $this->response = new ResponseBody;
        parent::__construct($id, $module, $config);
    }
}