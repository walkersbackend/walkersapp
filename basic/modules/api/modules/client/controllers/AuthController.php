<?php

namespace app\modules\api\modules\client\controllers;

use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\services\auth\AbstractWalkerAuthService;
use app\modules\api\modules\client\services\auth\adapter\email\WalkerAuthByEmailAdapter;
use app\modules\api\modules\client\services\auth\adapter\google\WalkerAuthByGoogleTokenAdapter;
use yii\filters\VerbFilter;

/**
 * Controller for authorize walker
 * @package app\modules\api\modules\client\controllers
 */
class AuthController extends BaseController
{
    /**
     * @var AbstractWalkerAuthService
     */
    private $walkerAuthService;

    /**
     * AuthController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param AbstractWalkerAuthService $walkerAuthService
     * @param array $config
     */
    public function __construct($id, $module, AbstractWalkerAuthService $walkerAuthService, $config = [])
    {
        $this->walkerAuthService = $walkerAuthService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Authorizes walker
     * @return array
     */
    public function actionIndex()
    {
        try {
            $walker = $this
                ->walkerAuthService
                    ->auth(
                        new WalkerAuthByEmailAdapter(
                            \Yii::$app->request->post()
                        )
                    );
            return $this->response
                ->setData(['token' => $walker->generateToken()])
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Authorizes walker
     * @return array
     */
    public function actionGToken()
    {
        try {
            $walker = $this
                ->walkerAuthService
                    ->auth(
                        new WalkerAuthByGoogleTokenAdapter(
                            \Yii::$app->request->post()
                        )
                    );
            return $this->response
                ->setData(['token' => $walker->generateToken()])
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    '*' => ['post'],
                ],
            ],
        ];
    }
}