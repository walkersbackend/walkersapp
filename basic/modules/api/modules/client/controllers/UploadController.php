<?php

namespace app\modules\api\modules\client\controllers;

use app\modules\api\models\history\History;
use app\modules\api\models\task\TaskResource;
use app\modules\api\models\track\Track;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\walker\NotFoundCurrentTaskException;
use app\modules\api\modules\client\services\auth\AbstractWalkerAuthService;
use app\modules\api\modules\client\services\auth\adapter\token\WalkerAuthByTokenAdapter;
use app\modules\api\modules\client\services\upload\file\FileUploader;
use app\modules\api\modules\client\services\upload\location\LocationValidatorService;
use app\modules\api\modules\client\services\upload\resource\ResourceValidatorService;
use Yii;
use yii\filters\VerbFilter;

/**
 * Controller for upload resources, file and locations
 * @package app\modules\api\modules\client\controllers
 */
class UploadController extends BaseController
{
    /**
     * @var AbstractWalkerAuthService
     */
    private $walkerAuthService;

    /**
     * @var ResourceValidatorService
     */
    private $resourceValidatorService;

    /**
     * @var LocationValidatorService
     */
    private $locationValidatorService;

    /**
     * UploadController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param AbstractWalkerAuthService $walkerAuthService
     * @param ResourceValidatorService $resourceValidatorService
     * @param LocationValidatorService $locationValidatorService
     * @param array $config
     */
    public function __construct($id, $module,
                                AbstractWalkerAuthService $walkerAuthService,
                                ResourceValidatorService $resourceValidatorService,
                                LocationValidatorService $locationValidatorService,
                                array $config = [])
    {
        $this->walkerAuthService = $walkerAuthService;
        $this->resourceValidatorService = $resourceValidatorService;
        $this->locationValidatorService = $locationValidatorService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Upload resource
     * @return array
     */
    public function actionResource()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            $history = History::getCurrentHistory($walker);
            if (!$history) {
                throw new NotFoundCurrentTaskException;
            }

            $resources = $this->resourceValidatorService->validate($data);
            TaskResource::removeAndAddResources($resources, $history->task_id, $walker->id);

            return $this->response->setSuccess(true)->getAsArray();
        } catch (MyException $exception) {
            return $this->response->setError($exception->getErrorBody())->getAsArray();
        }
    }
    /**
     * Upload location
     * @return array
     */
    public function actionLocation()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            if (!$history = History::getCurrentHistory($walker)) {
                throw new NotFoundCurrentTaskException;
            }

            $locations = $this->locationValidatorService->validate($data);
            Track::uploadLocations($locations, $history);

            return $this->response->setSuccess(true)->getAsArray();
        } catch (MyException $exception) {
            return $this->response->setError($exception->getErrorBody())->getAsArray();
        }
    }

    /**
     * Upload location
     * @return array
     */
    public function actionFile()
    {
        try {
            return $this->response
                ->setSuccess(true)
                ->setData(['id' => (new FileUploader())->upload($_FILES)])
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->setSuccess(false)
                ->getAsArray();
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    '*' => ['post'],
                ],
            ],
        ];
    }
}