<?php

namespace app\modules\api\modules\client\controllers;

use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\services\auth\AbstractWalkerAuthService;
use app\modules\api\modules\client\services\auth\adapter\token\WalkerAuthByTokenAdapter;
use app\modules\api\modules\client\services\task\provider\info\HistoryInfoProvider;
use yii\base\Module;
use yii\filters\VerbFilter;

/**
 * Class HistoryController
 * @package app\modules\api\modules\client\controllers
 */
class HistoryController extends BaseController
{
    /**
     * @var AbstractWalkerAuthService
     */
    protected $walkerAuthService;

    public function __construct($id, Module $module, AbstractWalkerAuthService $walkerAuthService, array $config = [])
    {
        $this->walkerAuthService = $walkerAuthService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Return rejected tasks list for walker
     * @return array
     */
    public function actionRejectedList()
    {
        try {
            $walker = $this
                ->walkerAuthService
                    ->auth(
                        new WalkerAuthByTokenAdapter(
                            \Yii::$app->request->post()
                        )
                    );
            return $this->response
                ->setSuccess(true)
                ->setData(['tasks' => (new HistoryInfoProvider())->getTasks($walker->id)])
                ->getAsArray();
        } catch (MyException $e) {
            return $this->response
                ->setSuccess(false)
                ->setError($e->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    '*' => ['post'],
                ],
            ],
        ];
    }
}