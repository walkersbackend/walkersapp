<?php

namespace app\modules\api\modules\client\controllers;

use app\modules\api\models\constants\status\Status;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\exceptions\task\TaskAlreadyTakenException;
use app\modules\api\modules\client\exceptions\walker\WalkerAlreadyPerformException;
use app\modules\api\modules\client\services\auth\AbstractWalkerAuthService;
use app\modules\api\modules\client\services\auth\adapter\token\WalkerAuthByTokenAdapter;
use app\modules\api\modules\client\services\task\accept\RejectedTaskAssigner;
use app\modules\api\modules\client\services\task\assign\AvailableTaskAssigner;
use app\modules\api\modules\client\services\task\cancel\PerformedTaskCancellationService;
use app\modules\api\modules\client\services\task\reject\RejectTaskCancellationService;
use app\modules\api\modules\client\services\task\provider\info\TaskInfoProvider;
use app\modules\api\modules\client\services\task\back\ITaskReturnerService;
use app\modules\api\modules\client\services\task\complete\ITaskCompletionService;
use app\modules\api\modules\client\services\task\find\ITaskFinderService;
use Yii;
use yii\filters\VerbFilter;

/**
 * Controller for task actions
 * @package app\modules\api\modules\client\controllers
 * Class for
 */
class TaskController extends BaseController
{
    /**
     * @var AbstractWalkerAuthService
     */
    private $walkerAuthService;

    /**
     * @var ITaskFinderService
     */
    private $taskFinderService;

    /**
     * @var ITaskCompletionService
     */
    private $taskCompletionService;

    /**
     * @var ITaskReturnerService
     */
    private $taskReturnerService;


    /**
     * TaskController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param AbstractWalkerAuthService $walkerAuthService
     * @param ITaskFinderService $taskFinderService
     * @param ITaskCompletionService $taskCompletionService
     * @param ITaskReturnerService $taskReturnerService
     * @param array $config
     */
    public function __construct($id, $module,
                                AbstractWalkerAuthService $walkerAuthService,
                                ITaskFinderService $taskFinderService,
                                ITaskCompletionService $taskCompletionService,
                                ITaskReturnerService $taskReturnerService,
                                $config = [])
    {
        $this->walkerAuthService = $walkerAuthService;
        $this->taskFinderService = $taskFinderService;
        $this->taskCompletionService = $taskCompletionService;
        $this->taskReturnerService = $taskReturnerService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Authorizes walker by email
     * @return array
     */
    public function actionList()
    {
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        \Yii::$app->request->post()
                    )
                );
            if (History::getCurrentHistory($walker)) {
                throw new WalkerAlreadyPerformException;
            }

            return $this->response
                ->setData(['tasks' => Task::getTasks($walker)])
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Assigns a walker to a task
     * @return array
     */
    public function actionAssign()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            if (History::getCurrentHistory($walker)) {
                throw new WalkerAlreadyPerformException;
            }

            $task = $this->taskFinderService->find($data);

            if ($task->status == Status::AVAILABLE) {
                (new AvailableTaskAssigner($walker, $task))->assign();
            } elseif ($task->status == Status::REJECTED) {
                (new RejectedTaskAssigner($walker, $task))->assign();
            } else {
                throw new TaskAlreadyTakenException($task);
            }

            return $this->response
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Assigns a walker to a rejected task
     * @return array
     */
    public function actionAccept()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            if (History::getCurrentHistory($walker)) {
                throw new WalkerAlreadyPerformException;
            }

            $task = $this->taskFinderService->find($data);

            (new RejectedTaskAssigner($walker, $task))->assign();

            return $this->response
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Reject a walker to from a rejected task
     * @return array
     */
    public function actionReject()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            $task = $this->taskFinderService->find($data);
            (new RejectTaskCancellationService($walker, $task, $data['reason'] ?? null))->cancel();

            return $this->response
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Find current task, which walker is perform
     * @return array
     */
    public function actionCurrent()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            $history = History::getCurrentHistory($walker);
            $task = empty($history) ? null : $history->task;
            $task = empty($task) ? null : $task->getWithResources($walker->id, $history->start_time);
            return $this->response
                ->setSuccess(true)
                ->setData(['task' => $task])
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Cancels a task
     * @return array
     */
    public function actionCancel()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            $reason = isset($data['reason']) ? $data['reason'] : null;
            (new PerformedTaskCancellationService($walker, null, $reason))->cancel();
            return $this->response
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Complete a task
     * @return array
     */
    public function actionComplete()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            $this->taskCompletionService->complete($walker);
            return $this->response
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Return info about a task
     * @return array
     */
    public function actionInfo()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            $task = $this->taskFinderService->find($data);
            if (
                !$task->isCanBeTakenAsAvailable($walker->level) &&
                !$task->isCanBeTakenAsRejected($walker->id)
            ) {
                throw new TaskAlreadyTakenException($task);
            }

            return $this->response
                ->setSuccess(true)
                ->setData(['task' => $task->getWithResources()])
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Return info about a tasks
     * @return array
     */
    public function actionData()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            $srv = new TaskInfoProvider();

            return $this->response
                ->setSuccess(true)
                ->setData(['tasks' => $srv->getTasks($walker, $data)])
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * @return array
     */
    public function actionBack()
    {
        $data = Yii::$app->request->post();
        try {
            $walker = $this
                ->walkerAuthService
                ->auth(
                    new WalkerAuthByTokenAdapter(
                        $data
                    )
                );
            if (History::getCurrentHistory($walker)) {
                throw new WalkerAlreadyPerformException;
            }

            $task = $this->taskReturnerService->back($walker);
            return $this->response
                ->setSuccess(true)
                ->setData(['task' => $task])
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    '*' => ['post'],
                ],
            ],
        ];
    }
}