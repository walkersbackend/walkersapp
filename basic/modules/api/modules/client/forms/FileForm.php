<?php

namespace app\modules\api\modules\client\forms;

use yii\base\Model;

/**
 * Class for validate file
 * @package app\modules\api\modules\client\forms
 */
class FileForm extends Model
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $type;

    /**
     * @var string
     */
    public $tmp_name;

    /**
     * @var int
     */
    public $size;

    /**
     * Rules for validate a file
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'type', 'tmp_name', 'size'], 'required'],
            ['size', 'integer'],
            [['tmp_name', 'name'], 'string'],
            ['type', 'match', 'pattern' => '/(audio|video|image)\//']
        ];
    }

    /**
     * Return extension of a file
     * @return string
     */
    public function getExtension()
    {
        return '.' . preg_replace('/.+\./', '', $this->name);
    }
}