<?php

namespace app\modules\api\modules\client\forms;

use yii\base\Model;

/**
 * Class for validate email
 * @package app\modules\api\modules\client\models
 */

class EmailForm extends Model
{
    /**
     * @var string
     */
    public $email;

    /**
     * Custom rules for email field
     * @return array
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            [['email'], 'email'],
            ['email', 'match', 'pattern' => '/@gmail.com/'],
        ];
    }
}