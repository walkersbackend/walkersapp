<?php

namespace app\modules\api\modules\client\forms;

use app\modules\api\models\definitions\Definitions;
use yii\base\Model;

/**
 * Class TokenForm
 * Class for validate token and return Walker
 * @package app\modules\api\modules\client\models
 */
class TokenForm extends Model
{
    /**
     * @var string
     */
    public $token;

    /**
     * Custom rules for token field
     * @return array
     */
    public function rules()
    {
        return [
            ['token', 'required'],
            ['token', 'string', 'length' => Definitions::TOKEN_LENGTH],
        ];
    }
}