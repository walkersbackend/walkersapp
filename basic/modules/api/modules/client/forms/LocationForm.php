<?php

namespace app\modules\api\modules\client\forms;

use yii\base\Model;

/**
 * Class for validate locations
 * @package app\modules\api\modules\client\forms
 */
class LocationForm extends Model
{
    /**
     * @var double
     */
    public $latitude;

    /**
     * @var double
     */
    public $longitude;

    /**
     * @var int
     */
    public $time;

    /**
     * Custom rules for location
     * @return array
     */
    public function rules()
    {
        return [
            [['latitude', 'longitude', 'time'], 'required'],
            [['latitude', 'longitude'], 'match', 'pattern' => '/^\-?(\d{1,3}(\.\d+)?)$/'],
            [['latitude'], 'double', 'min' => -90, 'max' => 90],
            [['longitude'], 'double', 'min' => -180, 'max' => 180],
            [['time'], 'integer', 'min' => 0]
        ];
    }

    /**
     * Clear model
     */
    public function clear()
    {
        $this->latitude = null;
        $this->longitude = null;
        $this->time = null;
        $this->clearErrors();
    }
}