<?php

namespace app\modules\api\modules\client\forms;

use app\modules\api\models\constants\resource\Resource;
use app\modules\api\models\task\TaskResource;
use app\modules\api\modules\client\exceptions\resource\ResourcesNotLoadedException;
use Yii;
use yii\base\Model;

/**
 * Class for base validating resource
 * @package app\modules\api\modules\client\forms
 */
class ResourcesForm extends Model
{
    /**
     * Text comment
     * @var string
     */
    public $comment;

    /**
     * Array of content
     * @var
     */
    public $content;

    /**
     * @var array
     */
    public $images;

    /**
     * @var string
     */
    public $field_id;

    /**
     * @var string
     */
    public $field_name;

    /**
     * Custom rules for resource
     * @return array
     */
    public function rules()
    {
        return [
            [['field_id', 'field_name'], 'required'],
            [['comment', 'field_id', 'field_name'], 'string'],
            ['field_id', 'in', 'range' => Resource::getFieldIds()],
            [['images', 'content'], 'default', 'value' => null],
            [['images'], 'each', 'rule' => ['string']],
            [['comment'] , 'default', 'value' => null]
        ];
    }

    /**
     * Checks links
     * @throws ResourcesNotLoadedException
     */
    public function afterValidate()
    {
        if (!$this->hasErrors()) {
            $path = Yii::getAlias('@uploadsGlobal') . '/';
            $pattern = '/'. preg_quote($path, '/') .'\w*\.(jpg|jpeg)/';
            $arr = $this->images;
            if (!empty($arr)) {
                foreach ($arr as $res) {
                    if (empty($res)) {
                        continue;
                    }
                    if (!preg_match($pattern, $res)) {
                        throw new ResourcesNotLoadedException;
                    }
                }
            }
        }
    }

    /**
     * Get resource object as array, when content implodes to string
     * @return array
     */
    public function getAsArray()
    {
        return [
            'content' => $this->content,
            'comment' => $this->comment,
            'field_id' => $this->field_id,
            'field_name' => $this->field_name,
            'images' => $this->images
        ];
    }

    /**
     * Clear model fields
     */
    public function clear()
    {
        $this->content = null;
        $this->comment = null;
        $this->field_id = null;
        $this->field_name = null;
        $this->images = null;

        $this->clearErrors();
    }

    /**
     * Prepare resources for validate
     * @return bool
     */
    public function beforeValidate()
    {
        if(!is_array($this->images)) {
            $this->images = json_decode($this->images);
        }
        $this->content = TaskResource::parseContent($this);
        return parent::beforeValidate();
    }
}

