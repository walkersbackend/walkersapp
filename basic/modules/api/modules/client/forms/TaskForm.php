<?php

namespace app\modules\api\modules\client\forms;

use yii\base\Model;

/**
 * Class TaskForm
 * @package app\modules\api\modules\client\models
 * Class for validate id and return Task
 */

class TaskForm extends Model
{
    /**
     * @var int
     */
    public $task_id;

    /**
     * Custom rules for task_id field
     * @return array
     */
    public function rules()
    {
        return [
            ['task_id', 'required'],
            ['task_id', 'integer', 'min' => 1],
        ];
    }
}