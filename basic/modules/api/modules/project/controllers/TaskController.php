<?php

namespace app\modules\api\modules\project\controllers;

use app\modules\api\models\history\History;
use app\modules\api\models\task\TaskResource;
use app\modules\api\modules\client\controllers\BaseController;
use app\modules\api\modules\client\exceptions\main\MyException;
use app\modules\api\modules\client\services\task\find\TaskFinderService;
use app\modules\api\modules\client\exceptions\task\TaskNotCompletedException;
use app\modules\api\modules\project\services\task\MonitoringService;
use app\modules\api\modules\project\services\task\TaskAcceptService;
use app\modules\api\modules\project\services\task\TaskCreator;
use app\modules\api\modules\client\services\upload\resource\ResourceValidatorService;
use app\modules\api\modules\project\services\task\TaskRejectService;
use app\modules\api\modules\project\services\task\TaskRemover;
use Yii;
use yii\filters\VerbFilter;

/**
 * Class TaskController
 * @package app\modules\api\modules\project
 */
class TaskController extends BaseController
{
    /**
     * @var ResourceValidatorService
     */
    private $resourceValidatorService;

    /**
     * @param string $id
     * @param \yii\base\Module $module
     * @param ResourceValidatorService $resourceValidatorService
     * @param array $config
     */
    public function __construct($id, $module, ResourceValidatorService $resourceValidatorService, array $config = [])
    {
        $this->resourceValidatorService = $resourceValidatorService;
        parent::__construct($id, $module, $config);
    }

    /**
     * Action for create s task
     * @return array
     */
    public function actionCreate()
    {
        try {
            $posts = Yii::$app->request->post();
            $id = (new TaskCreator)->create($posts);
            if (!empty($posts['resources'])) {
                TaskResource::removeAndAddResources(
                    $this->resourceValidatorService
                        ->setSkipInvalidResources(true)
                        ->validate($posts),
                    $id
                );
            }
            return $this->response
                ->setSuccess(true)
                ->setData(['id' => $id])
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setSuccess(false)
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Action for create s task
     * @return array
     */
    public function actionUpdate()
    {
        try {
            $posts = Yii::$app->request->post();
            $task = (new TaskFinderService)->find($posts);
            (new TaskCreator)->update($task, $posts);
            if (!empty($posts['resources'])) {
                TaskResource::removeAndAddResources(
                    $this->resourceValidatorService
                        ->setSkipInvalidResources(true)
                        ->validate($posts),
                    $task->id
                );
            }
            return $this->response
                ->setSuccess(true)
                ->setData(['id' => $task->id])
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setSuccess(false)
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Task reopening method
     * @return array
     */
    public function actionReject()
    {
        try {
            $task = (new TaskFinderService)->find($data = Yii::$app->request->post());
            (new TaskRejectService())->reject($task, $data['reason'] ?? null);
            return $this->response->setSuccess(true)->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setSuccess(false)
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Return monitoring info
     * @return array
     */
    public function actionMonitoring()
    {
        try {
            $info = (new MonitoringService)->monitoring(Yii::$app->request->get());
            return $this->response
                ->setSuccess(true)
                ->setData($info)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setSuccess(false)
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Action for return result of a task
     * @return array
     * @throws TaskNotCompletedException
     */
    public function actionResult()
    {
        try {
            $task = (new TaskFinderService)->find(Yii::$app->request->get());
            $result = History::getResult($task);
            if (!$result) {
                throw new TaskNotCompletedException;
            }

            return $this->response
                ->setSuccess(true)
                ->setData($result)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setSuccess(false)
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Action for remove a task
     * @return array
     */
    public function actionRemove()
    {
        try {
            $task = (new TaskFinderService)->find(Yii::$app->request->post());
            (new TaskRemover())->remove($task);
            return $this->response
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setSuccess(false)
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * Action for accept a task
     * @return array
     */
    public function actionAccept()
    {
        try {
            $task = (new TaskFinderService)->find(Yii::$app->request->post());
            (new TaskAcceptService())->accept($task);
            return $this->response
                ->setSuccess(true)
                ->getAsArray();
        } catch (MyException $exception) {
            return $this->response
                ->setSuccess(false)
                ->setError($exception->getErrorBody())
                ->getAsArray();
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'monitoring' => ['get'],
                    'result' => ['get'],
                    'create' => ['post'],
                    'update' => ['post'],
                    'remove' => ['post'],
                    'reopen' => ['post'],
                    'accept' => ['post'],
                    'reject' => ['post'],
                ],
            ],
        ];
    }
}