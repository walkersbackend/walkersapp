<?php

namespace app\modules\api\modules\project\controllers;

use app\modules\api\modules\client\controllers\BaseController;
use app\modules\api\modules\project\services\walker\WalkerFindService;
use app\modules\api\modules\project\services\walker\WalkerList;
use yii\filters\VerbFilter;

/**
 * Class WalkerController
 * @package app\modules\api\modules\project\controllers
 */
class WalkerController extends BaseController
{
    /**
     * @var WalkerFindService
     */
    private $service;

    /**
     * WalkerController constructor.
     * @param string $id
     * @param \yii\base\Module $module
     * @param WalkerFindService $walkerFindService
     * @param array $config
     */
    public function __construct($id, $module, WalkerFindService $walkerFindService, array $config = [])
    {
        $this->service = $walkerFindService;
        parent::__construct($id, $module, $config);
    }

    /**
     * @return array
     */
    public function actionList()
    {
        $schema = ['id', 'name', 'email'];
        return $this->response
            ->setSuccess(true)
            ->setData(['walkers' => $this->service->find(new WalkerList($schema))])
            ->getAsArray();
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'list' => ['get'],
                ],
            ],
        ];
    }
}