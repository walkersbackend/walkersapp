<?php

namespace app\modules\api\modules\project\services\walker;

/**
 * Interface IWalkerFindService
 * @package app\modules\api\modules\project\services\walker
 */
interface IWalkerFind
{
    /**
     * @return mixed
     */
    public function find();
}