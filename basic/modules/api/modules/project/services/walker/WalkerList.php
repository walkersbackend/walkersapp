<?php

namespace app\modules\api\modules\project\services\walker;

use app\modules\api\models\walker\Walker;
use yii\helpers\ArrayHelper;

/**
 * Class WalkerList
 * @package app\modules\api\modules\project\services\walker
 */
class WalkerList implements IWalkerFind
{
    /**
     * @var null
     */
    private $schema = null;

    /**
     * WalkerList constructor.
     * @param null $schema
     */
    public function __construct($schema = null)
    {
        if (!$schema) return;
        $this->schema = $schema;
    }

    /**
     * @return array
     */
    public function find()
    {
        return $this->findBySchema();
    }

    /**
     * @return array
     */
    private function findBySchema()
    {
        $walkers = Walker::find()->select($this->schema)->all();
        return ArrayHelper::toArray($walkers);
    }
}