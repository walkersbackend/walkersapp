<?php

namespace app\modules\api\modules\project\services\walker;

/**
 * Class WalkerFindService
 * @package app\modules\api\modules\project\services\walker
 */
class WalkerFindService
{
    /**
     * @param IWalkerFind $service
     * @return mixed
     */
    public function find(IWalkerFind $service)
    {
        return $service->find();
    }
}