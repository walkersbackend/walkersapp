<?php

namespace app\modules\api\modules\project\services\task;
use app\modules\api\models\constants\result\Result;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use yii\helpers\ArrayHelper;

/**
 * Class MonitoringService
 * @package app\modules\api\modules\project\services\task
 */
class MonitoringService
{
    /**
     * Return id of completed task since given timestamp, and new timestamp
     * @param $data array
     * @return array
     */
    public function monitoring($data)
    {
        $time = time() - Task::RESERVE_TIME;

        $timestamp = isset($data['timestamp']) ? $data['timestamp'] : 0;

        $condition = [
            'AND',
            ['result' => Result::MODERATED],
            ['>=', 'end_time', $timestamp],
            ['<', 'end_time', $time]
        ];
        $histories = History::find()->where($condition)->all();
        $histories = ArrayHelper::getColumn(ArrayHelper::toArray($histories), 'task_id');

        return [
            'timestamp' => $time,
            'list' => $histories
        ];
    }
}