<?php

namespace app\modules\api\modules\project\services\task;

use app\modules\api\models\task\Task;
use app\modules\api\modules\client\exceptions\task\TaskNotCreateException;

/**
 * Class TaskCreator
 * @package app\modules\api\modules\project\services\task
 */
class TaskCreator {

    /**
     * Create a task and return id of creating task
     * @param array $post
     * @return int
     * @throws TaskNotCreateException
     */
    public function create($post) {
        $model = (new Task())->setDefaultFields();
        if (!$model->load($post, '') || !$model->save()) {
            throw new TaskNotCreateException;
        }
        return $model->id;
    }

    /**
     * @param Task $task
     * @param array $data
     * @throws TaskNotCreateException
     */
    public function update($task, $data) {
        if (!$task->load($data, '') || $task->update() === false) {
            throw new TaskNotCreateException;
        }
    }
}