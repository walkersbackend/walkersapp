<?php

namespace app\modules\api\modules\project\services\task;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\constants\status\Status;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\modules\project\exceptions\TaskCannotBeReOpenedException;

class TaskRejectService
{
    /**
     * Reject the task that walker performed
     * @param Task $task
     * @param null|string $comment
     * @throws TaskCannotBeReOpenedException
     */
    public function reject($task, $comment = null)
    {
        if ($task->status != Status::MODERATED) {
            throw new TaskCannotBeReOpenedException();
        }

        $history = History::getPerformingHistory($task->id, $task->status);

        if ($history) {
            $history->updateAttributes([
                'result' => Result::REJECTED,
                'start_time' => time(),
                'end_time' => null,
                'comment' => $comment
            ]);

            if (!$task->walker_id) {
                $updateAttributes['walker_id'] = $history->walker_id;
            }
        }

        $task->updateAttributes([
            'status' => Status::REJECTED,
            'delete_date' => null,
        ]);
    }
}