<?php

namespace app\modules\api\modules\project\services\task;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\constants\status\Status;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use app\modules\api\modules\client\exceptions\task\TaskNotCompletedException;

/**
 * Class TaskAcceptService
 * @package app\modules\api\modules\project\services\task
 */
class TaskAcceptService
{
    /**
     * @param Task $task
     * @throws TaskNotCompletedException
     */
    public function accept($task)
    {
        if (!in_array($task->status, [Status::MODERATED])) {
            throw new TaskNotCompletedException($task);
        }

        $history = History::getPerformingHistory($task->id, $task->status);

        if (!$history) {
            throw new TaskNotCompletedException($task);
        }

        $history->updateAttributes([
            'result' => Result::COMPLETED
        ]);

        $task->updateAttributes([
            'status' => Status::COMPLETED
        ]);
    }
}