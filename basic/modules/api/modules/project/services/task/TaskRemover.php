<?php

namespace app\modules\api\modules\project\services\task;

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;

/**
 * Class TaskRemover
 * @package app\modules\api\modules\project\services\task
 */
class TaskRemover
{
    /**
     * Mark task as removed, and remove walker's history by this task
     * @param Task $task
     */
    public function remove($task)
    {
        if ($task->isDeleted()) {
            return;
        }

        if ($history = History::getPerformingHistory($task->id,$task->status)) {
            $history->updateAttributes([
                'result' => Result::CANCELED,
                'end_time' => time(),
                'comment' => 'Задача была удалена'
            ]);
        }

        $task->markAsDeleted();
    }
}