<?php

namespace app\modules\api\modules\project\exceptions;

use app\modules\api\models\errors\Errors;
use app\modules\api\modules\client\exceptions\main\MyException;

class TaskCannotBeReOpenedException extends MyException
{
    /**
     * TaskAlreadyTakenException constructor.
     */
    public function __construct()
    {
        $this->errorBody = Errors::getBody(Errors::TASK_CANNOT_BE_REOPENED);
    }
}