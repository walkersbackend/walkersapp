<?php

use yii\db\Migration;

class m180320_064818_add_last_activity_column_into_table_walker extends Migration
{
    /**
     * @var string
     */
    protected  $table = 'walker';

    /**
     * @var string
     */
    protected $column = 'last_activity';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn(
            $this->table,
            $this->column,
            $this->integer()
                ->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn(
            $this->table,
            $this->column
        );
    }
}
