<?php

use yii\db\Migration;

class m170911_094815_remove_filed_resource_from_task_resource extends Migration
{
    /**
     * @var string
     */
    private $table = 'task_resource';

    /**
     * @var string
     */
    private $field = 'resource';

    /**
     * @inheritdoc
     */
    public function up() {
        $this->dropColumn($this->table, $this->field);
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->addColumn(
            $this->table,
            $this->field,
            $this->integer()->notNull()
        );
    }
}
