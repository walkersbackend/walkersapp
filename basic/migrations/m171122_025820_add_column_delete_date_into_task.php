<?php

use yii\db\Migration;

/**
 * Class m171122_025820_add_column_delete_date_into_task
 */
class m171122_025820_add_column_delete_date_into_task extends Migration
{
    /**
     * @var string
     */
    private $table = 'task';

    /**
     * @var string
     */
    private $column = 'delete_date';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(
            $this->table,
            $this->column,
            $this->integer()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(
            $this->table,
            $this->column
        );
    }
}
