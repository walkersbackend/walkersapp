<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m170421_150051_create_task_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'level' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull(),
            'title' => $this->string(255)->notNull(),
            'info' => $this->text()->notNull(),
            'link' => $this->string()->defaultValue(null),
            'email' => $this->string()->defaultValue(null),
            'address' => $this->string(),
            'latitude' => $this->float()->notNull(),
            'longitude' => $this->float()->notNull(),
            'cost' => $this->decimal(19,2)->notNull(),
            'date' => $this->integer()->notNull(),
            'release_date' => $this->integer()->notNull(),
            'time' => $this->integer()->notNull()
        ]);

        $defaultIssue = '
        - уточнить местоположение, если компании нет по указанному адресу, вернуть пустые ресурсы и комментарий, что компании нет
        - уточнить время работы (фото, либо текст, если фото нету)
        - получить контакт (ФИО + телефон) для дальнейших регулярных обновлений по телефону
        - сделать фотографию здания
        - сделать фотографию входа в здание
        - сделать фотографию входа в компанию';
        $date = time();
        $duration = \app\modules\api\models\definitions\Definitions::TASK_EXECUTION_TIME;
        $sql = "INSERT INTO task (id, level, status, title, info, link, email, address, latitude, longitude, cost, date, release_date, time) VALUES
        (1, 1, 1, 'Сервис Групп (Торгово-сервисная компания)', '{$defaultIssue}', 'http://www.vl.ru/servis-group', 'sg.info@mail.ru', 'Владивосток, район \"Некрасовская\", ул. Крылова, 10 оф. 307', 56.4713, 84.9508, 100.00, $date, $date, $duration),
        (2, 1, 1, 'Твоя Картошка (Пит-стоп)', '{$defaultIssue} \n WhatsApp : +79024839130', 'http://www.vl.ru/tvoya-kartoshka', NULL, 'Владивосток, район \"Первая речка\", пр-кт. Острякова 2', 56.4664, 84.977, 100.00, $date, $date, $duration),
        (3, 1, 1, 'Книжник (Книжный магазин)', '{$defaultIssue}', 'http://www.vl.ru/knizhnik', NULL, 'Владивосток, район \"Некрасовская\", пр-кт Красного Знамени', 56.4696, 84.9507, 100.00, $date, $date, $duration),
        (4, 1, 1, 'Уссури (Продуктовый магазин)',  '{$defaultIssue}', 'http://www.vl.ru/grand-stroy', NULL, 'Владивосток, район \"Центр\", пр-кт Красного Знамени', 56.472, 84.9511, 100.00, $date, $date, $duration),
        (5, 1, 1, 'Гранд-Строй (Строительная компания)',  '{$defaultIssue}\n Комментарий: узнать про адрес ул. Нейбута, 33 и телефон 79020685336 (не отвечают)', 'http://www.vl.ru/katelya', NULL, 'Владивосток, район \"Центр\", пр-кт. Партизанский 40', 56.4807, 84.9816, 100.00, $date, $date, $duration),
        (6, 1, 1, 'Katelya (Цветочный салон)',  '{$defaultIssue} \n Комментарий: контакт 8-908-459-18-49 (Татьяна, директор) не отвечает на звонки.', 'http://www.vl.ru/prod-ussuri', NULL, 'Владивосток, район \"Первая речка\", пр-кт Красного Знамени, 31 стр. 2', 56.4813, 84.9814, 100.00, $date, $date, $duration)";
        $this->execute($sql);

    }

    /**
     * @inheritdoc
     */
    public function down() {

        $this->dropTable('task');
    }
}
