<?php

use yii\db\Migration;

class m170623_024449_alter_table_task extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('task', 'latitude', $this->decimal(10,7)->notNull());
        $this->alterColumn('task', 'longitude', $this->decimal(10,7)->notNull());
    }

    public function safeDown()
    {
        echo "m170623_024449_alter_table_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170623_024449_alter_table_task cannot be reverted.\n";

        return false;
    }
    */
}
