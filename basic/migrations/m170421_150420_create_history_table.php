<?php

use yii\db\Migration;

/**
 * Handles the creation of table `history`.
 */
class m170421_150420_create_history_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {

        $this->createTable('history', [
            'id' => $this->primaryKey(),
            'walker_id' => $this->integer()->notNull(),
            'task_id' => $this->integer()->notNull(),
            'result' => $this->integer()->defaultValue(null),
            'start_time' => $this->integer()->notNull(),
            'end_time' => $this->integer()->defaultValue(null),
            'comment' => $this->string()->defaultValue(null)
        ]);

        $this->createIndex(
            'end_time_start_time',
            'history',
            ['end_time', 'start_time']
        );

        $this->addForeignKey(
            'fk_history_walker_id',
            'history',
            'walker_id',
            'walker',
            'id'
        );

        $this->addForeignKey(
            'fk_history_task_id',
            'history',
            'task_id',
            'task',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down() {

        $this->dropIndex(
            'end_time_start_time',
            'history'
        );

        $this->dropForeignKey(
            'fk_history_walker_id',
            'history'
        );

        $this->dropForeignKey(
            'fk_history_task_id',
            'history'
        );

        $this->dropTable('history');
    }
}
