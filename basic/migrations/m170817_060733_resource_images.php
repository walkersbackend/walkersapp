<?php

use yii\db\Migration;

/**
 * @see http://jira.dev/browse/WLK-41
 */
class m170817_060733_resource_images extends Migration {
    /**
     * @var string
     */
    private $table = 'task_resource';
    /**
     * @var string
     */
    private $field = 'images';

    /**
     * @inheritdoc
     */
    public function up() {
        $this->addColumn(
            $this->table,
            $this->field,
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropColumn($this->table, $this->field);
    }
}