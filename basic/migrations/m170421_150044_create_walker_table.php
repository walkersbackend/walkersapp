<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m170421_150044_create_walker_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {

        $this->createTable('walker', [
            'id' => $this->primaryKey(),
            'level' => $this->integer()->notNull(),
            'name' => $this->string(50)->notNull(),
            'email' => $this->string('50')->notNull(),
            'number' => $this->string()->notNull(),
            'date' => $this->integer(),
            'token' => $this->string(32)->defaultValue(null)
        ]);

        $date = time();
        $sql = "INSERT INTO walker (id, level, name, email, number, date, token) VALUES (NULL, 1, 'Иван Салосин', 'salonkasoli@gmail.com', '8746465465', $date, NULL)";
        $this->execute($sql);


    }

    /**
     * @inheritdoc
     */
    public function down() {

        $this->dropTable('walker');

    }
}
