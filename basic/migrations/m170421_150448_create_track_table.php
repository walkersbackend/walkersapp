<?php

use yii\db\Migration;

/**
 * Handles the creation of table `track`.
 */
class m170421_150448_create_track_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('track', [
            'id' => $this->primaryKey(),
            'history_id' => $this->integer()->notNull(),
            'latitude' => $this->float(4)->notNull(),
            'longitude' => $this->float(4)->notNull(),
            'date' => $this->integer()->notNull()
        ]);

        $this->addForeignKey(
            'fk_track_history_id',
            'track',
            'history_id',
            'history',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down() {

        $this->dropForeignKey(
            'fk_track_history_id',
            'track'
        );

        $this->dropTable('track');
    }
}
