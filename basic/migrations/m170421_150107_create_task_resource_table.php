<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task_resource`.
 */
class m170421_150107_create_task_resource_table extends Migration {
    /**
     * @inheritdoc
     */
    public function up() {
        $this->createTable('task_resource', [
            'id' => $this->primaryKey(),
            'task_id' => $this->integer()->notNull(),
            'walker_id' => $this->integer()->notNull(),
            'resource' => $this->integer()->notNull(),
            'content' => $this->text()->defaultValue(null),
            'comment' => $this->text()->defaultValue(null),
        ]);

        $this->addForeignKey(
            'fk_task_resource_task_id',
            'task_resource',
            'task_id',
            'task',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_task_resource_walker_id',
            'task_resource',
            'walker_id',
            'walker',
            'id'
        );

        $sql = "";
        $this->execute($sql);
    }

    /**
     * @inheritdoc
     */
    public function down() {

        $this->dropForeignKey(
            'fk_task_resource_task_id',
            'task_resource'
        );

        $this->dropForeignKey(
            'fk_task_resource_walker_id',
            'task_resource'
        );

        $this->dropTable('task_resource');
    }
}
