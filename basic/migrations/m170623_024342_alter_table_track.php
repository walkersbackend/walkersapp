<?php

use yii\db\Migration;

class m170623_024342_alter_table_track extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('track', 'latitude', $this->decimal(10,7)->notNull());
        $this->alterColumn('track', 'longitude', $this->decimal(10,7)->notNull());
    }

    public function safeDown()
    {
        echo "m170623_024342_alter_table_track cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170623_024342_alter_table_track cannot be reverted.\n";

        return false;
    }
    */
}
