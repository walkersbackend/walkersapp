<?php

use yii\db\Migration;

/**
 * Class m171207_060730_add_column_user_agent_into_table_walker
 */
class m171207_060730_add_column_user_agent_into_table_walker extends Migration
{
    /**
     * @var string
     */
    private $table = 'walker';

    /**
     * @var string
     */
    private $column = 'user_agent';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(
            $this->table,
            $this->column,
            $this->text()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn(
            $this->table,
            $this->column
        );
    }
}
