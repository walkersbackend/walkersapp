<?php

use yii\db\Migration;

class m171226_024048_add_column_walker_id_into_table_task extends Migration
{
    private $table = 'task';

    private $column = 'walker_id';

    public function up()
    {
        $this->addColumn(
            $this->table,
            $this->column,
            $this->integer()->defaultValue(null)
        );

        $this->addForeignKey(
            'fk_task_walker_id',
            $this->table,
            $this->column,
            'walker',
            'id'
        );

        $status = \app\modules\api\models\constants\status\Status::AVAILABLE;
        $result = \app\modules\api\models\constants\result\Result::COMPLETED;

        $sql = "UPDATE task AS t1 RIGHT JOIN (SELECT task.id AS id, history.walker_id AS walker_id
            FROM task, history WHERE status != $status AND task.id = history.task_id
            AND history.start_time IS NOT NULL AND (history.result = $result OR history.end_time IS NULL)) t2
            ON t1.id = t2.id SET t1.walker_id = t2.walker_id;
        ";

        $this->execute($sql);
    }

    public function down()
    {
        $this->dropForeignKey('fk_task_walker_id', $this->table);

        $this->dropColumn($this->table, $this->column);
    }
}
