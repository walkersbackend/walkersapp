<?php

use yii\db\Migration;

class m170626_094556_modify_column_info_in_task extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('task', 'info', $this->text()->defaultValue(null));
    }

    public function safeDown()
    {
        echo "m170626_094556_modify_column_info_in_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170626_094556_modify_column_info_in_task cannot be reverted.\n";

        return false;
    }
    */
}
