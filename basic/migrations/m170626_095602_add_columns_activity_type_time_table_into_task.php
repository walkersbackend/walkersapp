<?php

use yii\db\Migration;

class m170626_095602_add_columns_activity_type_time_table_into_task extends Migration
{
    public function safeUp()
    {
        $this->addColumn('task','activity_type',$this->string(50)->defaultValue(null));
        $this->addColumn('task','time_table',$this->string(255)->defaultValue(null));
    }

    public function safeDown()
    {
        echo "m170626_095602_add_columns_activity_type_time_table_into_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m170626_095602_add_columns_activity_type_time_table_into_task cannot be reverted.\n";

        return false;
    }
    */
}
