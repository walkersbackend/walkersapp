<?php

use app\modules\api\models\constants\result\Result;
use app\modules\api\models\constants\status\Status;
use app\modules\api\models\history\History;
use app\modules\api\models\task\Task;
use yii\db\Migration;

class m180202_111609_update_old_rejected_tasks extends Migration
{
    public function safeUp()
    {
        $tasks = Task::find()->where([
                'AND',
                ['status' => Status::AVAILABLE],
                ['not', ['walker_id' => null]]
            ])->all();

        if (!empty($tasks)) {

            $time = time();

            /** @var Task $task */
            foreach ($tasks as $task) {
                $task->updateAttributes(['status' => Status::REJECTED]);

                new History([
                    'result' => Result::REJECTED,
                    'start_time' => $time,
                    'end_time' => null,
                    'comment' => $task->info,
                    'task_id' => $task->id,
                    'walker_id' => $task->walker_id
                ]);
            }
        }

        echo count($tasks) . " tasks was updated" . PHP_EOL;
    }

    public function safeDown()
    {

    }
}
