<?php

use yii\db\Migration;

/**
 * Class m171220_040642_add_column_walker_user_agent_into_table_history
 */
class m171220_040642_add_column_walker_user_agent_into_table_history extends Migration
{
    /**
     * @var string
     */
    private $table = 'history';

    /**
     * @var string
     */
    private $column = 'walker_user_agent';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn(
            $this->table,
            $this->column,
            $this->string()->defaultValue(null)
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->table, $this->column);
    }
}
