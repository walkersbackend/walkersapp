<?php

use yii\db\Migration;

/**
 * @see http://jira.dev/browse/WLK-41
 */
class m170810_025122_specifying_resources extends Migration {
    /**
     * @var string
     */
    private $taskResourceTable = 'task_resource';
    /**
     * @var string
     */
    private $taskTable = 'task';

    /**
     * @inheritdoc
     */
    public function up() {
        $this->addColumn(
            $this->taskResourceTable,
            'field_id',
            $this->typeString(50)
        );
        $this->addColumn(
            $this->taskResourceTable,
            'field_name',
            $this->typeString(255)
        );
        $this->alterColumn(
            $this->taskResourceTable,
            'walker_id',
            $this->integer()->defaultValue(null)
        );

        $this->dropColumn($this->taskTable, 'email');
        $this->dropColumn($this->taskTable, 'activity_type');
        $this->dropColumn($this->taskTable, 'time_table');
    }

    /**
     * @inheritdoc
     */
    public function down() {
        $this->dropColumn($this->taskResourceTable, 'field_id');
        $this->dropColumn($this->taskResourceTable, 'field_name');

        $this->addColumn(
            $this->taskTable,
            'activity_type',
            $this->typeString(50)
        );
        $this->addColumn(
            $this->taskTable,
            'time_table',
            $this->typeString(255)
        );
        $this->addColumn(
            $this->taskTable,
            'email',
            $this->typeString()
        );
        $this->alterColumn(
            $this->taskResourceTable,
            'walker_id',
            $this->integer()->notNull()
        );
    }

    /**
     * @param int $length
     * @return yii\db\ColumnSchemaBuilder
     */
    private function typeString($length = null) {
        return $this
            ->string($length)
            ->defaultValue(null);
    }
}
