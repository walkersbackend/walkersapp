<?php

namespace app\commands;

use app\modules\api\trash\TrashCleaner;
use yii\console\Controller;

/**
 * Class FileCleanerController
 * @package app\commands
 */
class FileCleanerController extends Controller
{
    /**
     * Run removing old files
     */
    public function actionIndex()
    {
        $path = require(__DIR__. '/../config/params.php');
        (new TrashCleaner())->clean($path['localUploadDirectory']);
    }
}