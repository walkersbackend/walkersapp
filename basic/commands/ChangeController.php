<?php

namespace app\commands;

use app\modules\api\models\task\TaskResource;
use yii\console\Controller;

class ChangeController extends Controller
{
    /**
     * Action, which run reloading tasks
     */
    public function actionIndex()
    {
        $field1 = 'schedule';
        $field2 = 'content';
        $condition = [
            'and',
            ['!=', $field2, "[\"\"]"],
            ['field_id' => $field1]
        ];
        $resources = TaskResource::find()->where($condition)->all();

        foreach ($resources as $res) {
            $_content = TaskResource::parseContent($res);
            $res->content = json_encode($_content);
            $res->save(false);
        }
    }
}