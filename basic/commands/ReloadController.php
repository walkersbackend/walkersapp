<?php

namespace app\commands;

use app\modules\api\reload\TaskReload;
use yii\console\Controller;

/**
 * Class ReloadController
 * @package app\commands
 */
class ReloadController extends Controller
{
    /**
     * Action, which run reloading tasks
     */
    public function actionIndex()
    {
        (new TaskReload())->run();
    }
}
